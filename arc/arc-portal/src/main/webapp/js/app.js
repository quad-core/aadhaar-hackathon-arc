var app = angular.module('arc', [ 'ngRoute', 'ngCookies', 'datatables',
		'ngDialog' ]);
app.config(function($routeProvider) {
	$routeProvider.when('/home', {
		templateUrl : 'templates/home.html',
		controller : 'HomeController',
		permission:'*'
	}).when('/login', {
		templateUrl : 'templates/login.html',
		controller : 'LoginController',
		permission:'?'
	}).when('/register', {
		templateUrl : 'templates/register.html',
		controller : 'RegisterController',
		permission:'?'
	}).when('/merchants', {
		templateUrl : 'templates/merchant/list.html',
		controller : 'ListMerchantsController',
		permission:['admin']
	}).when('/merchant/create', {
		templateUrl : 'templates/merchant/form.html',
		controller : 'CreateMerchantController',
		permission:['admin']
	}).when('/merchant/edit/:id', {
		templateUrl : 'templates/merchant/form.html',
		controller : 'EditMerchantController',
		permission:['admin']
	}).when('/requests', {
		templateUrl : 'templates/requests.html',
		controller : 'RequestsController',
		permission:['citizen']
	}).otherwise({
		redirectTo : '/home'
	});
});
app.run( function($rootScope, $location, CookieService) {
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
    	if(next && next.permission){
	    	if(next.permission=='*'){
	    		CookieService.checkSession();
	    	}
	    	else if(next.permission=='?'){
	    		CookieService.isSessionExists();
	    	}
	    	else if(next.permission.indexOf(CookieService.getState('role'))==-1){
	    		$location.path('home');
	    	}
    	}
    });
 });
app.controller("MainController", function($scope, $rootScope, CookieService,
		$location, ngDialog, AuthService) {
	$scope.header = true;
	$scope.showMenu = false;
	$scope.showHeader = function(flag) {
		$scope.header = flag;
		$scope.loggedInUser = CookieService.getState('name');
		if(CookieService.isCitizen()){
			$scope.showMenu = true;
		}
		else{
			$scope.showMenu = false;
		}
	};
	$scope.passwords = {};
	$scope.logout = function() {
		CookieService.clear();
		$location.path('login');
	};
	$scope.fnChangePassword = function(){
		$scope.changePwdModal = ngDialog
		.open({
			template : 'templates/changePassword.html',
			scope : $scope,
			className : 'ngdialog-theme-plain custom-width'
		});
	};
	$scope.changePassword = function(){
		var def = AuthService.changePassword($scope.passwords.old, $scope.passwords.newPassword);
		def.then(function(response){
			alert('Password Changed successfully');
			$scope.passwords = {};
			$scope.changePwdModal.close();
		}, function(error){
			alert(error);
		});
	};
});

app.controller("LoginController", function($scope, $rootScope, $location,
		AuthService, CookieService) {
	$scope.showHeader(false);
	$scope.username = $scope.password = "";
	$scope.doLogin = function() {
		var def = AuthService.doLogin($scope.username, $scope.password);
		def.then(function(response) {
			if (response.status == "success") {
				CookieService.setState('sessionId', new Date());
				CookieService.setState('uid', response.uid);
				CookieService.setState('role', response.role);
				CookieService.setState('name', response.name);
				$location.path('home');
			} else {
				alert(response.error);
			}
		}, function(error) {
			alert(error);
		});
	};
});
app.controller("RegisterController", function($scope, $rootScope, AuthService,
		CookieService, ngDialog) {
	$scope.showHeader(false);
	$scope.user = {};
	$scope.doRegister = function() {
		var def = AuthService.doRegister($scope.user);
		def.then(function(response) {
			if (response.status == "success") {
				$scope.user = {};
				alert('Registered Successfully');
				$scope.openOTPDialog();
			} else {
				alert(response.error);
			}
		}, function(error) {
			alert(error);
		});
	};
	$scope.openOTPDialog = function(){
		$scope.otp = {};
		$scope.OTPDialog = ngDialog
		.open({
			template : 'templates/OTPDialog.html',
			scope : $scope,
			className : 'ngdialog-theme-plain'
		});
	};
	$scope.verifyOTP = function(){
		var def = AuthService.verifyOTP($scope.otp);
		def.then(function(response){
			alert('OTP Verified. Account activated');
			$scope.OTPDialog.close();
		}, function(error){
			alert(error);
		});
	};
});
app
		.controller(
				"ListMerchantsController",
				function($scope, $rootScope, MerchantService, CookieService,
						DTOptionsBuilder, DTColumnBuilder, $compile, $location) {
					$scope.fnEditMerchant = function(id) {
						$location.path('merchant/edit/' + id);
					};
					$scope.fnCreateMerchant = function() {
						$location.path('merchant/create');
					};
					$scope.fnDeleteMerchant = function(id, name) {
						if (confirm("are you sure you want to delete the merchant("
								+ name + ")?")) {
							var def = MerchantService.deleteMerchantById(id);
							def.then(function(data) {
								alert('Merchant deleted successfully');
								$scope.dtInstance.reloadData();
							}, function(error) {
								alert(error);
							});
						}
					};
					$scope.dtOptions = DTOptionsBuilder
							.fromSource(
									'http://localhost/learnings/json/he/getAllMerchants.php')
							.withOption(
									'fnRowCallback',
									function(nRow, aData, iDisplayIndex,
											iDisplayIndexFull) {
										$compile(nRow)($scope);
									})
							.withOption(
									'fnDrawCallback',
									function(oSettings) {
										angular.element('#merchant-toolbar')
												.addClass('toolbar pull-left');
										angular
												.element('#merchant-toolbar')
												.html(
														'<div class="btn-group" role="group">'
																+ '<button type="button" class="btn btn-success" ng-click="fnCreateMerchant()"><i class="fa fa-plus-circle"></i> Add Merchant</button>'
																+ '<button type="button" class="btn btn-info" ng-click="reloadData()"><i class="fa fa-refresh"></i> Refresh</button>'
																+ '</div>');
										$compile(
												angular.element(
														'#merchant-toolbar')
														.contents())($scope);
									}).withOption('dom',
									'<"#merchant-toolbar">frtip')
							.withPaginationType('full_numbers');
					$scope.dtColumns = [
							DTColumnBuilder.newColumn('name').withTitle('Name'),
							DTColumnBuilder.newColumn('username').withTitle(
									'Username'),
							DTColumnBuilder.newColumn('email').withTitle(
									'Email Address'),
							DTColumnBuilder
									.newColumn('id')
									.withTitle('')
									.notSortable()
									.renderWith(
											function(data, type, full) {
												var button = '<div class="btn-group pull-right" role="group">'
														+ '<button class="btn btn-default" ng-click="fnEditMerchant('
														+ data
														+ ')"><i class="fa fa-edit text-success"></i></button>'
														+ '<button class="btn btn-default" ng-click="fnDeleteMerchant('
														+ data
														+ ', \''
														+ full.name
														+ '\')"><i class="fa fa-trash text-danger"></i></button>'
														+ '</div>';

												return button;
											}) ];
					$scope.reloadData = reloadData;
					$scope.dtInstance = {};
					function reloadData() {
						var resetPaging = false;
						$scope.dtInstance.reloadData(callback, resetPaging);
					}
					;
					function callback(json) {
						console.log(json);
					}
					;
				});
app.controller("CreateMerchantController", function($scope, $rootScope,
		MerchantService, CookieService, $compile, $location) {
	$scope.mode = 'Create';
	$scope.merchant = {};
	$scope.save = function() {
		var def = MerchantService.createMerchant($scope.merchant);
		def.then(function(response) {
			alert('Merchant added successfully');
			$location.path('merchants');
		}, function(error) {
			alert(error);
		});
	};
});
app.controller("EditMerchantController", function($scope, $rootScope,
		MerchantService, CookieService, $compile, $location, $routeParams) {
	$scope.merchantId = $routeParams.id;
	$scope.mode = 'Edit';
	$scope.merchant = {};
	var def = MerchantService.getMerchantById($scope.merchantId);
	def.then(function(response) {
		$scope.merchant = response;
	}, function(error) {
		alert(error);
	});
	$scope.save = function() {
		var def = MerchantService.updateMerchant($scope.merchant);
		def.then(function(response) {
			alert('Merchant updated successfully');
			$location.path('merchants');
		}, function(error) {
			alert(error);
		});
	};
});
app.controller('RequestsController', function($scope, $rootScope, CookieService, $location, $routeParams, DocumentService,
		DTOptionsBuilder, DTColumnBuilder, $compile){
	$scope.fnApprove = function(id){
		var def = DocumentService.approveRequestById(id);
		def.then(function(response){
			alert('Request approved successfully');
			$scope.requestsOptions.reloadData();
		}, function(error){
			alert(error);
		});
	};
	$scope.fnReject = function(id){
		var def = DocumentService.rejectRequestById(id);
		def.then(function(response){
			alert('Request rejected successfully');
			$scope.requestsOptions.reloadData();
		}, function(error){
			alert(error);
		});
	};
	/**************** START-----Requests datatable *******/
	$scope.requestsOptions = {};
	$scope.requestsOptions.dtOptions = DTOptionsBuilder
			.fromSource(
					'http://localhost/learnings/json/he/getAllRequests.php')
			.withOption(
					'fnRowCallback',
					function(nRow, aData, iDisplayIndex,
							iDisplayIndexFull) {
						$compile(nRow)($scope);
					})
			.withOption(
					'fnDrawCallback',
					function(oSettings) {
						angular
								.element(
										'#requests-toolbar')
								.addClass(
										'toolbar pull-left');
						angular
								.element(
										'#requests-toolbar')
								.html(
										'<button type="button" class="btn btn-info" ng-click="requestsOptions.reloadData()"><i class="fa fa-refresh"></i> Refresh</button>');
						$compile(
								angular
										.element(
												'#requests-toolbar')
										.contents())
								($scope);
					}).withOption('dom',
					'<"#requests-toolbar">frtip')
			.withPaginationType('simple_numbers');
	$scope.requestsOptions.dtColumns = [
			DTColumnBuilder.newColumn('sender').withTitle(
					'Sender'),
			DTColumnBuilder.newColumn('documentTypeList').withTitle(
					'List of document types'),
			DTColumnBuilder.newColumn('date').withTitle(
					'Date'),
			DTColumnBuilder.newColumn('status').withTitle(
					'Status'),
					DTColumnBuilder
					.newColumn('id')
					.withTitle('')
					.notSortable()
					.renderWith(
							function(data, type, full) {
								var button = '<div class="btn-group pull-right" role="group">'
									+ '<button class="btn btn-success" ng-click="fnApprove('
									+ data
									+ ')"><i class="fa fa-thumbs-o-up"></i> Approve</button>'
									+ '<button class="btn btn-danger" ng-click="fnReject('
									+ data+')"><i class="fa fa-thumbs-o-down"></i> Reject</button>'
									+ '</div>';
								return button;
							})
	];
	$scope.requestsOptions.dtInstance = {};
	$scope.requestsOptions.reloadData = function(){
		var resetPaging = true;
		$scope.requestsOptions.dtInstance.reloadData(callback, resetPaging);
	}
	$scope.requestsOptions.dtInstanceCallback = function(dtInstance) {
		$scope.requestsOptions.dtInstance = dtInstance;
	};
	function callback(json) {
		console.log(json);
	};
	/**************** END-----Requests datatable *******/
});

/*********** START---- File Upload ***********/
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.service('fileUpload', ['$q', '$http', function ($q, $http) {
    this.uploadFileToUrl = function(file, uploadUrl){
    	var def = $q.defer();
        var fd = new FormData();
        fd.append('file', file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(response){
        	if(response.status=="success"){
        		def.resolve(response);
        	}
        	else{
        		def.reject(response.error);
        	}
        })
        .error(function(error){
        	def.reject(error);
        });
        return def.promise;
    }
}]);
/*********** END---- File Upload ***********/