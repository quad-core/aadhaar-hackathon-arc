package quad.arc.core.model;

import quad.arc.core.util.DataBean;

public class DocumentStoreRequest extends DataBean {

    private static final long serialVersionUID = 1L;

    private String name;
    
    private String uri;
    
    private String aadhaarNo;
    
    private String biometricDataInBase64;

    public DocumentStoreRequest() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    public String getBiometricDataInBase64() {
        return biometricDataInBase64;
    }

    public void setBiometricDataInBase64(String biometricData) {
        this.biometricDataInBase64 = biometricData;
    }
}
