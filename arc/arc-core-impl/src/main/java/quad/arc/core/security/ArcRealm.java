package quad.arc.core.security;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.util.JdbcUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quad.arc.core.model.Role;
import quad.arc.core.util.LangUtil;

public class ArcRealm extends JdbcRealm {
    private static final Logger logger = LoggerFactory.getLogger(ArcRealm.class);
    
    private static final String GET_ROLE_NAMES_FOR_USER = "select ACCOUNT_ROLE from ARC_ACCOUNT where LOGIN_NAME = ?";
    
	private static final String GET_PASSWORD = "select PASSWORD from ARC_ACCOUNT where LOGIN_NAME = ?";
	
	private static final Map<Role, Set<String>> rolePermissions = new TreeMap<>();
	static {
	    rolePermissions.put(Role.ADMIN, new TreeSet<String>(Arrays.asList("merchant:manage")));
	    rolePermissions.put(Role.CITIZEN, new TreeSet<String>(Arrays.asList("merchant:manage","document:share","document-request:view","document-request:respond")));
	    rolePermissions.put(Role.MERCHANT, new TreeSet<String>(Arrays.asList("document:store","document:view","document-request:create","document-request:view")));
	}
	
    protected String jndiDataSourceName;
    
    public ArcRealm() {
        super();
        this.permissionsLookupEnabled = true;
        this.setCredentialsMatcher(new ArcCredentialsMatcher());
    }

    public String getJndiDataSourceName() {
        return jndiDataSourceName;
    }

    public void setJndiDataSourceName(String jndiDataSourceName) {
        this.jndiDataSourceName = jndiDataSourceName;
        this.dataSource = getDataSourceFromJNDI(jndiDataSourceName);
    }

    private DataSource getDataSourceFromJNDI(String jndiDataSourceName) {
        try {
            InitialContext ic = new InitialContext();
            return (DataSource) ic.lookup(jndiDataSourceName);
        } catch (NamingException e) {
            logger.error(String.format("JNDI error while retrieving %s",jndiDataSourceName), e);
            throw new AuthorizationException(e);
        }
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken t = (UsernamePasswordToken)token;
        
        String username = t.getUsername();
        
        if (LangUtil.isEmpty(username)) {
            throw new AccountException("Null or empty username");
        }
        
        AuthenticationInfo info = null;
        
        Connection connection = null;
        
        try {
            connection = this.dataSource.getConnection();
            
            PreparedStatement ps = null;
            ps = connection.prepareStatement(GET_PASSWORD);
            
            try {
                ps.setString(1, username);
                ResultSet rs = ps.executeQuery();
                
                try {
                    if (rs.next()) {
                        String password = rs.getString("password");
                        info = new SimpleAuthenticationInfo(username, password.toCharArray(), this.getName());
                    }
                } finally {
                    JdbcUtils.closeResultSet(rs);
                }
            } finally {
                JdbcUtils.closeStatement(ps);
            }
        } catch (SQLException e) {
            logger.error("Unexpected error while fetching authentication info", e);
        } finally {
            JdbcUtils.closeConnection(connection);
        }
        
        return info;
    }

    @Override
    protected Set<String> getRoleNamesForUser(Connection connection, String username) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Set<String> roleNames = new HashSet<String>();
        
        try {
            ps = connection.prepareStatement(GET_ROLE_NAMES_FOR_USER);
            ps.setString(1, username);
            
            rs = ps.executeQuery();
            
            while (rs.next()) {
                roleNames.add(rs.getString(1));
            }
        } finally {
            JdbcUtils.closeResultSet(rs);
            JdbcUtils.closeStatement(ps);
        }
         
        return roleNames;
    }

    @Override
    protected Set<String> getPermissions(Connection connection, String username,   Collection<String> roleNames) throws SQLException {
        Set<String> permissions = null;
        
        if (roleNames.isEmpty()) {
            permissions = Collections.emptySet();
            return permissions;
        }
        
        permissions = rolePermissions.get(roleNames.iterator().next()); // As there will be only Role
        
        logger.info("Permissions of User[{}] is {}", username, permissions);
        return permissions;
    }
}