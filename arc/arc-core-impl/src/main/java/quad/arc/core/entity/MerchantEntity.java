package quad.arc.core.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import quad.arc.core.util.AbstractEntity;

@Entity
@Table(name="ARC_MERCHANT", 
       uniqueConstraints={@UniqueConstraint(name="UNIQUE_CONSTRAINT",
                                            columnNames={"MERCHANT_NAME", "IS_TRASHED", "UPDATED_ON"})})
public class MerchantEntity extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String name;

	private List<MerchantUserEntity> merchantUsers;
	
	private List<DocumentEntity> receivedDocuments;
    
	public MerchantEntity() {
		super();
	}

    @Column(name="ARC_MERCHANT_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="MERCHANT_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy="merchant", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @OnDelete(action=OnDeleteAction.CASCADE)
    public List<MerchantUserEntity> getMerchantUsers() {
        return merchantUsers;
    }

    public void setMerchantUsers(List<MerchantUserEntity> merchantUsers) {
        this.merchantUsers = merchantUsers;
    }

    @OneToMany(mappedBy="submittedTo", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @OnDelete(action=OnDeleteAction.CASCADE)
    public List<DocumentEntity> getReceivedDocuments() {
        return receivedDocuments;
    }

    public void setReceivedDocuments(List<DocumentEntity> receivedDocuments) {
        this.receivedDocuments = receivedDocuments;
    }
}
