package quad.arc.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import quad.arc.core.model.Role;
import quad.arc.core.util.AbstractEntity;

@Entity
@Table(name="ARC_ACCOUNT", 
       uniqueConstraints={@UniqueConstraint(name="UNIQUE_CONSTRAINT",
                                            columnNames={"LOGIN_NAME", "IS_TRASHED", "UPDATED_ON"})})
public class AccountEntity extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Long id;
	
    private String loginName;
    
    private String password;
    
    private Role role;
    
	public AccountEntity() {
		super();
	}

    @Column(name="ARC_ACCOUNT_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="LOGIN_NAME")
    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    @Column(name="PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name="ACCOUNT_ROLE")
    @Enumerated(EnumType.STRING)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
