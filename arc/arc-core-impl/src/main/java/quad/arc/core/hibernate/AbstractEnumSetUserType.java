package quad.arc.core.hibernate;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Set;
import java.util.TreeSet;

import org.aspectj.util.LangUtil;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

public class AbstractEnumSetUserType<T extends Enum<?>> implements UserType{

    private Class<T> enumClass;

    public AbstractEnumSetUserType(Class<T> enumClass) {
        super();
        this.enumClass = enumClass;
    }

    @Override
    public int[] sqlTypes() {
        return new int[]{Types.VARCHAR};
    }

    @Override
    public Class returnedClass() {
        return Set.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        if(x == y){
            return true;   
        }if(x == null || y ==null){
            return false;   
        }
        return x.equals(y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
        String value = rs.getString(names[0]);
        if (LangUtil.isEmpty(value)) {
            return null;
        }
        
        String [] tokens = value.split(",");
        
        Set<T> enumSet = new TreeSet<>();
        for(String token:tokens) {
            T enumValue = null;

            String name = token.trim();
            
            try {
                Method method = this.enumClass.getMethod("valueOf", String.class);
                enumValue = (T)method.invoke(null, name);
            } catch (Exception e) {
                // TODO Handle this exception appropriately
                e.printStackTrace();
            }
                
            enumSet.add(enumValue);
        }
        
        return enumSet;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
        if(value == null) {
            st.setNull(index, Types.VARCHAR);
            return;
        }
        
        if(!(value instanceof Set))
        {
            throw new UnsupportedOperationException();
        }

        Set<T> enumSet = (Set<T>)value;
        if (enumSet.isEmpty()) {
            st.setNull(index, Types.VARCHAR);
            return;
        }
        
        StringBuffer tokenBuffer = new StringBuffer();
        for(T v : enumSet) {
            tokenBuffer.append(",").append(v.name());
        }
        
        tokenBuffer.deleteCharAt(0);
        st.setString(index, tokenBuffer.toString());
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable)value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
} 