<!Doctype html>
<html>
<head>
<title>ARC</title>
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-theme.min.css">
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/js/angular/angular.min.js"></script>
<script src="${pageContext.request.contextPath}/js/angular/angular-route.min.js"></script>
<script src="${pageContext.request.contextPath}/js/angular/angular-cookies.min.js"></script>
<script src="${pageContext.request.contextPath}/js/angular/angular-resource.min.js"></script>
<script src="${pageContext.request.contextPath}/js/angular/ngDialog/js/ngDialog.min.js"></script>
<script src="${pageContext.request.contextPath}/js/angular/dataTable/angular-datatables.min.js"></script>
<script src="${pageContext.request.contextPath}/js/angular/dataTable/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/js/angular/dataTable/dataTables.bootstrap.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/angular/dataTable/dataTables.bootstrap.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/angular/ngDialog/css/ngDialog.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/angular/ngDialog/css/ngDialog-theme-default.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/angular/ngDialog/css/ngDialog-theme-plain.min.css" />
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css">
<script type="text/javascript">
var arc_contextPath = "${pageContext.request.contextPath}";
</script>
</head>
<body ng-app="arc" ng-class="{login:!header}" ng-controller="MainController">
	<div class="navbar navbar-default">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#navbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<span class="navbar-brand">ARC</span>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav" ng-if="showMenu">
				<li><a href="#home">Dashboard</a></li>
				<li><a href="#requests">Approval Requests</a></li>				
			</ul>			
			<span class="pull-right"> Welcome <span ng-bind="loggedInUser"></span>, 
				<a href="javascript:void(0)" ng-click="fnChangePassword()">Change Password</a> - 
				<a href="javascript:void(0)"
				class="text-danger" ng-click="logout();">Logout</a>
			</span>
		</div>
	</div>
	<div class="container-fluid">
		<div ng-view=""></div>
	</div>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/app.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/services.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/HomeController.js"></script>
</body>
</html>