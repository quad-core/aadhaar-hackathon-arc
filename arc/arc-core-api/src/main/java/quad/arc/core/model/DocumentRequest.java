package quad.arc.core.model;

import java.util.Set;

import quad.arc.core.util.DataBean;

public class DocumentRequest extends DataBean {
    private static final long serialVersionUID = 1L;
    
    private Set<DocumentType> documentTypes;
    
    private CitizenResponseCode responseCode;
    
    public DocumentRequest() {
        super();
    }

    public Set<DocumentType> getDocumentTypes() {
        return documentTypes;
    }

    public void setDocumentTypes(Set<DocumentType> documentTypes) {
        this.documentTypes = documentTypes;
    }

    public CitizenResponseCode getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(CitizenResponseCode responseCode) {
        this.responseCode = responseCode;
    }
}
