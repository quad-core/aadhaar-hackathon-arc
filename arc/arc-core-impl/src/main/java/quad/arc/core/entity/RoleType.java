package quad.arc.core.entity;

import javax.persistence.EnumType;

import quad.arc.core.hibernate.AbstractEnumType;
import quad.arc.core.model.Role;

public class RoleType extends AbstractEnumType<Role>{
    public RoleType() {
        super(EnumType.ORDINAL, Role.class);
    }
}