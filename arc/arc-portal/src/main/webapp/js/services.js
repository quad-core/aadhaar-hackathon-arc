app.service('CookieService', function($rootScope, $cookieStore, $location) {
	this.checkSession = function() {
		if (!$cookieStore.get('sessionId')) {
			$location.path('login');
		}
	};
	this.isSessionExists = function() {
		if ($cookieStore.get('sessionId')) {
			$location.path('home');
		}
	};
	this.setState = function(key, value) {
		$cookieStore.put(key, value);
	};
	this.getState = function(key){
		return $cookieStore.get(key);
	};
	function checkRole(role) {
		var sessionRole = $cookieStore.get('role');
		if (sessionRole && sessionRole == role) {
			return true;
		}
		return false;
	}
	;
	this.isAdmin = function() {
		return checkRole('admin');
	};
	this.isCitizen = function() {
		return checkRole('citizen');
	};
	this.isMerchant = function() {
		return checkRole('merchant');
	};
	this.clear = function() {
		$cookieStore.remove('sessionId');
		$cookieStore.remove('uid');
		$cookieStore.remove('role');
		$cookieStore.remove('name');
	};
});
/*app.service('RBAService', function(CookieService){
	this.checkSession()
});*/
app.service('AuthService', function($q, $http, $cookieStore, $location) {
	this.doLogin = function(un, pwd) {
		var def = $q.defer();
		var req = {
			method : 'GET',
			/*headers : {
				'Content-Type' : 'application/json'
			},*/
			/*headers : {
				"content-type" : "application/x-www-form-urlencoded"
				},*/
			headers : {authorization : "Basic "
		        + btoa(un + ":" + pwd)
		    },
			url : arc_contextPath+'/rest/authn'
		};
		$http(req).success(function(data, status, headers, config) {
			def.resolve(data);
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.doRegister = function(user) {
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/register.php',
			data : user
		};
		$http(req).success(function(data, status, headers, config) {
			def.resolve(data);
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.changePassword = function(old, newPassword){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/changePassword.php',
			data : {
				'old' : old,
				'password' : newPassword
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.verifyOTP = function(otp){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/verifyOTP.php',
			data : {
				'aadharNo' : otp.aadharNo,
				'token' : otp.token
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
});
app.service('MerchantService', function($q, $http) {
	this.getAllMerchants = function() {
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/getAllMerchants.php'
		};
		$http(req).success(function(data, status, headers, config) {
			def.resolve(data);
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.createMerchant = function(merchant) {
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/createMerchant.php',
			data : merchant
		};
		$http(req).success(function(data, status, headers, config) {
			if (data.status == "success") {
				def.resolve(data);
			} else {
				def.reject(data.error);
			}
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.updateMerchant = function(merchant) {
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/updateMerchant.php',
			data : merchant
		};
		$http(req).success(function(data, status, headers, config) {
			if (data.status == "success") {
				def.resolve(data);
			} else {
				def.reject(data.error);
			}
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.getMerchantById = function(id) {
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/getMerchantById.php',
			data : {
				'id' : id
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if (data.status == "success") {
				def.resolve(data.merchant);
			} else {
				def.reject(data.error);
			}
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.deleteMerchantById = function(id) {
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/deleteMerchantById.php',
			data : {
				'id' : id
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if (data.status == "success") {
				def.resolve(data);
			} else {
				def.reject(data.error);
			}
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
});
app.service('DocumentService', function($q, $http) {
	this.getAllMyCertificates = function() {
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/getAllMyCertificates.php'
		};
		$http(req).success(function(data, status, headers, config) {
			def.resolve(data);
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.makeCertificateDefault = function(id){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/makeCertificateDefault.php',
			data : {
				'id' : id
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}			
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.getAllSignedDocuments = function() {
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/getAllSignedDocuments.php'
		};
		$http(req).success(function(data, status, headers, config) {
			def.resolve(data);
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.shareDocById = function(id, types){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/shareDocById.php',
			data : {
				'id' : id,
				'types':types
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}			
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.unshareDocById = function(id){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/unshareDocById.php',
			data : {
				'id' : id
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}			
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.sendRequest = function(request){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/sendRequest.php',
			data : request
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}			
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.approveRequestById = function(id){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/approveRequestById.php',
			data : {
				'id' : id
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}			
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.rejectRequestById = function(id){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/rejectRequestById.php',
			data : {
				'id' : id
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}			
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
	this.getDocumentSharedWithMeById = function(id){
		var def = $q.defer();
		var req = {
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			url : 'http://localhost/learnings/json/he/getDocumentSharedWithMeById.php',
			data : {
				'id' : id
			}
		};
		$http(req).success(function(data, status, headers, config) {
			if(data.status=="success"){
				def.resolve(data);
			}
			else{
				def.reject(data.error);
			}			
		}).error(function(data, status, headers, config) {
			def.reject(data);
		});
		return def.promise;
	};
});