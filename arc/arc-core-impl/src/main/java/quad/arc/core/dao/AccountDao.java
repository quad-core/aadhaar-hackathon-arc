package quad.arc.core.dao;

import quad.arc.core.entity.AccountEntity;

public interface AccountDao {
    public long save(AccountEntity accountEntity);
    
    public AccountEntity getAccountByLoginName(String loginName);
}