package quad.arc.scanner;

public interface FingerPrintScanner {
	
	public void capture(ScannerCallback callback);
	
	public void showMessage(String message);
	
}
