<script type="text/javascript">
	$(document).ready(function() {
		$('#merchants').dataTable({
			 "columnDefs": [ { orderable: false, targets: [3] }]
		});
		$('#btnAddMerchant').on('click', function(){
			document.merchantForm.reset();
			$('#mode').html('add');
			$('#addMerchant').modal();
		});
		$('#saveMerchant').on('click', function(e) {
			if($('#mode').html()=="edit"){
				updateMerchant();
			}
			else{
				addMerchant();
			}
			e.preventDefault();
		});
	}); 
</script>