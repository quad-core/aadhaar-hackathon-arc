package quad.arc.scanner;

public interface ScannerCallback {
	public void onCapture(String base64input);
	public String getHostName();
}
