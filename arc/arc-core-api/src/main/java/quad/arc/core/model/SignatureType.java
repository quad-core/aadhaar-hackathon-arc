package quad.arc.core.model;

public enum SignatureType {
    RUBBER_STAMP, DIGITAL_SIGNATURE
}
