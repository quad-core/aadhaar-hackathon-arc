package quad.arc.core.model;

import quad.arc.core.util.DataBean;

public class Signature extends DataBean {

    private static final long serialVersionUID = 1L;

    private String name;
    
    private String uri;
    
    private SignatureType signatureType;

    public Signature() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public SignatureType getSignatureType() {
        return signatureType;
    }

    public void setSignatureType(SignatureType signatureType) {
        this.signatureType = signatureType;
    }
}
