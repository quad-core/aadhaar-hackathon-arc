package quad.arc.core.service;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import quad.arc.core.model.Merchant;
import quad.arc.core.model.MerchantUser;

@Transactional(propagation = Propagation.REQUIRED)
public interface MerchantService {
    // merchant:manage
    public long addMerchant(Merchant merchant);
    
 // merchant:manage
    public void updateMerchant(Merchant merchant);
    
 // merchant:manage
    public Merchant getMerchantById(long merchantId);
    
 // merchant:manage
    public void deleteMerchant(long merchantId);
    
 // merchant:manage
    public List<Merchant> getMerchants();
    
 // merchant:manage
    public long addMerchantUser(MerchantUser merchantUser);
    
 // merchant:manage
    public void updateMerchantUser(MerchantUser merchantUser);
    
 // merchant:manage
    public MerchantUser getMerchantUserById(long merchantUserId);
    
 // merchant:manage
    public void deleteMerchantUser(long merchantUserId);

 // merchant:manage
    public List<MerchantUser> getMerchantUsers(long merchantId);
}