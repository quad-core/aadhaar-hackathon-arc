package quad.arc.core.security;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

import quad.arc.core.dao.AccountDao;
import quad.arc.core.entity.AccountEntity;
import quad.arc.core.model.Account;
import quad.arc.core.util.DataUtil;

public class Authenticator {

    @Autowired
    private AccountDao accountDao;
    
    public Authenticator() {
        super();
    }
    
    public void login(String loginName, String password) {
        UsernamePasswordToken token = new UsernamePasswordToken(loginName, ArcSecurityUtils.hash(password));
        Subject subject = SecurityUtils.getSubject();
        subject.login(token);
        
        AccountEntity accountEntity =  accountDao.getAccountByLoginName(loginName);
        Account account = new Account();
        account.setLoginName(accountEntity.getLoginName());
        account.setPassword(accountEntity.getPassword());
        account.setRole(accountEntity.getRole());
        DataUtil.setAuditDetails(accountEntity);
        
        Session session = subject.getSession();
        session.setAttribute(Account.class.getName(), account);
    }
    
    public void logout() {
        SecurityUtils.getSubject().logout();
    }
}
