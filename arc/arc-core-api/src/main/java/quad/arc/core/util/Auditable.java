package quad.arc.core.util;

import java.util.Date;

public interface Auditable {
    String getCreatedBy();

    void setCreatedBy(String createdBy);

    Date getCreatedOn();

    void setCreatedOn(Date createdOn);

    String getUpdatedBy();

    void setUpdatedBy(String updatedBy);

    Date getUpdatedOn();

    void setUpdatedOn(Date updatedOn);
}
