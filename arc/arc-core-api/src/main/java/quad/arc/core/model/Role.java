package quad.arc.core.model;

public enum Role {
    CITIZEN, MERCHANT, ADMIN
}