package quad.core.arc.desktop;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.io.File;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MainFrame extends JFrame {
    private static final long serialVersionUID = 1L;

    private List<File> selectedFiles;
    
    private JTextField txtAadhaar;
    
    private String biometricDataInBase64;
    
    public MainFrame() throws HeadlessException {
        super("Arc Client");
        this.init();
    }

    protected void init() {
        JTextArea txaSelectedFiles = new JTextArea(3,20);
        txaSelectedFiles.setEditable(false);
        JLabel lblFiles = new JLabel("Files");
        JButton btnBrowse = new JButton("Browse");
        
        JLabel lblAadhaar = new JLabel("Aadhaar No");
        JTextField txtAadhaar = new JTextField();
        JButton btnScan = new JButton("Scan");
        JButton btnSubmit = new JButton("Submit");
        
        JPanel pnlMain = new JPanel(new GridBagLayout());
        
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.gridwidth = 1;
        gbc.weightx = 0.0d;
        gbc.weighty = 0.0d;
        gbc.insets = new Insets(5,5,5,5);
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.NORTHEAST;
        
        pnlMain.add(lblFiles, gbc);
        
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JScrollPane(txaSelectedFiles), BorderLayout.CENTER);
        panel.add(btnBrowse,BorderLayout.EAST);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0d;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        pnlMain.add(panel, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 0.0d;
        gbc.fill = GridBagConstraints.NONE;
        pnlMain.add(lblAadhaar, gbc);
        
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1.0d;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        pnlMain.add(txtAadhaar, gbc);

        JPanel controlPanel = new JPanel();
        controlPanel.add(btnScan);
        controlPanel.add(btnSubmit);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 1.0d;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        pnlMain.add(controlPanel, gbc);
        
        this.getContentPane().add(pnlMain);
        
        this.pack();
        
        this.setSize(640, 480);
        
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
