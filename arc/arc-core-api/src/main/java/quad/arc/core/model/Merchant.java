package quad.arc.core.model;

import quad.arc.core.util.DataBean;

public class Merchant extends DataBean {
    private static final long serialVersionUID = 1L;
    
    private String name;
    
    public Merchant() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
