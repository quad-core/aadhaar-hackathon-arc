package quad.arc.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;

import quad.arc.core.model.DocumentType;
import quad.arc.core.util.AbstractEntity;

@Entity
@Table(name="ARC_DOCUMENT", 
       uniqueConstraints={@UniqueConstraint(name="UNIQUE_CONSTRAINT",
                                            columnNames={"DOC_UUID", "IS_TRASHED", "UPDATED_ON"})})
public class DocumentEntity extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String uuid;
	
    private String name;

    private DocumentType type;

    private MerchantEntity submittedTo;
    
    private CitizenEntity owner;
    
    private boolean shared;

	public DocumentEntity() {
		super();
	}

    @Column(name="ARC_DOCUMENT_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="DOC_UUID")
	public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Column(name="DOC_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="DOC_TYPE")
    @Enumerated(EnumType.STRING)
    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    @ManyToOne
    @JoinColumn(name="ARC_MERCHANT_ID")
    public MerchantEntity getSubmittedTo() {
        return submittedTo;
    }

    public void setSubmittedTo(MerchantEntity submittedTo) {
        this.submittedTo = submittedTo;
    }

    @ManyToOne
    @JoinColumn(name="ARC_CITIZEN_ID")
    public CitizenEntity getOwner() {
        return owner;
    }

    public void setOwner(CitizenEntity owner) {
        this.owner = owner;
    }
    
    @Type(type="org.hibernate.type.NumericBooleanType")
    @Column(name="IS_SHARED",nullable=false)
    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }
}
