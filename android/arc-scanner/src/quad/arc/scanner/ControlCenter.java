package quad.arc.scanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Savepoint;

import javax.net.ServerSocketFactory;

import android.util.Log;

public class ControlCenter {
    public static String startup(String bindAddress,FingerPrintScanner fingerP) throws Exception {
        Log.i("CC","Starting Control Center");
        ServerSocket serverSocket = ServerSocketFactory.getDefault().createServerSocket(8765, 5, InetAddress.getByName(bindAddress));
        
        Server server = new Server(serverSocket,fingerP);
        server.start();
        
        return serverSocket.getLocalSocketAddress().toString();
    }
    
    static class Server extends Thread {
        private ServerSocket serverSocket;
        FingerPrintScanner fingerP;
        public Server(ServerSocket serverSocket,FingerPrintScanner fingerP) {
            this.serverSocket = serverSocket;
            this.fingerP=fingerP;
        }

        @Override
        public void run() {
            try {
                Log.i("CC","Bind Address: " + serverSocket.getLocalSocketAddress());
                while(true) {
                    Socket socket = this.serverSocket.accept();
                    Log.i("CC", "Found a new client");
                    ClientService service = new ClientService(socket,fingerP);
                    service.start();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
            	fingerP.showMessage("Some Error Occured");
                e.printStackTrace();
            }
        }
    }
    
    static class ClientService extends Thread{
        private Socket socket;
        FingerPrintScanner fingerP;
        public ClientService(Socket socket,FingerPrintScanner fingerP) {
            this.socket = socket;
            this.fingerP=fingerP;
        }

        @Override
        public void run() {
        	BufferedReader in=null;
        	PrintWriter out=null;
        	try {
                Log.i("CC", "Client service started");
                in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
                out = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream()));
                String command = null;
                while((command=in.readLine())!=null) {
                    if ("quit".equals(command)) {
                        break;
                    }
                    
                    if("get".equals(command)){
                    	FingerPrintResponseHandler fpr = new FingerPrintResponseHandler(out,socket.getInetAddress().getHostAddress());
                    	fingerP.capture(fpr);
                    	continue;
                    }
                    out.println(String.format("Hello %s", command));
                    out.flush();
                }
                
            } catch (IOException e) {
                // TODO Auto-generated catch block
            	fingerP.showMessage("Some Error Occured");
                e.printStackTrace();
            }finally{
            	try{
            	out.close();
                in.close();
            	}catch(IOException ioe){}
            }
            
        }
        
    }
    
    static class FingerPrintResponseHandler implements ScannerCallback{

    	PrintWriter out;
    	String hostName;
    	
    	FingerPrintResponseHandler(PrintWriter out,String hostName){
    		this.out = out;
    		this.hostName = hostName;
    	}
    	
    	@Override
		public void onCapture(String base64input) {
			// TODO Auto-generated method stub
			out.println(base64input);
			out.flush();
		}

		@Override
		public String getHostName() {
			return hostName;
		}
    	
    	
    }
}
