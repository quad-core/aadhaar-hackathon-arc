package quad.core.arc.desktop;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import quad.arc.core.uiadiauth.UidaiAuthenticate;

	/**
	 * Example how to use multipart/form encoded POST request.
	 */
	public class FileUploadTester {
		private static String mailTo="";
		private static String uid = "";
		private static String filePath = "";
		   
		
	   public static void postFile(String filePath) throws Exception {
	       
	        CloseableHttpClient httpclient = HttpClients.createDefault();
	        try {
	            HttpPost httppost = new HttpPost("http://localhost:8080/arc-portal/rest/ext/repository");

	           // FileBody bin = new FileBody(new File("/Users/jkakka/git/aadhaar-hackathon-arc/arc/arc-desktop/src/main/java/quad/core/arc/desktop/Jignesh-Kakka-passport.png"));
	            FileBody bin = new FileBody(new File(filePath));
	            
	            StringBody comment = new StringBody("A binary file of some kind", ContentType.TEXT_PLAIN);

	            HttpEntity reqEntity = MultipartEntityBuilder.create()
	                    .addPart("bin", bin)
	                    .addPart("comment", comment)
	                    .build();


	            httppost.setEntity(reqEntity);

	            System.out.println("executing request " + httppost.getRequestLine());
	            CloseableHttpResponse response = httpclient.execute(httppost);
	            try {
	                System.out.println("----------------------------------------");
	                System.out.println(response.getStatusLine());
	                HttpEntity resEntity = response.getEntity();
	                if (resEntity != null) {
	                    System.out.println("Response content length: " + resEntity.getContentLength());
	                }
	                EntityUtils.consume(resEntity);
	            } finally {
	                response.close();
	            }
	        } finally {
	            httpclient.close();
	        }
	       
	    }
	    
	    

	   
	   public static boolean testAuth(String uid, String rawBiometric){
		   	UidaiAuthenticate uidaiAuth = new UidaiAuthenticate();
			boolean flag = uidaiAuth.authenticate(uid, rawBiometric);
			return flag;
	   }
	   
	 
	   public static void main(String[] args) throws Exception { 
		   String biometrics = "";
		   BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in)); 
		   System.out.println("Select Filepath");
		   filePath =  keyRead.readLine();
		   System.out.println("Enter UID");
		   uid = keyRead.readLine();
		   Socket sock = new Socket(args[0], 8765); // reading from keyboard (keyRead object) 
		   
		   //Socket sock = new Socket("10.127.142.12", 8765); // reading from keyboard (keyRead object) 
		   System.out.println("Scan Your Finger on mobile device");
		   // sending to client (pwrite object) 
		   OutputStream ostream = sock.getOutputStream(); 
		   PrintWriter pwrite = new PrintWriter(ostream, true);   // receiving from server ( receiveRead object) 
		   InputStream istream = sock.getInputStream(); 
		   String sendMessage; 
		   SocketInputReader sr = new SocketInputReader(istream);
		   sr.setInputCommand("get");
		   sr.start();
		   while(true) { 
			   sendMessage = keyRead.readLine(); // keyboard 
			   sr.setInputCommand(sendMessage);
			   if(sendMessage.equals("quit")){
				   sock.close();
				   break;
			   }
			   
			   pwrite.println(sendMessage); // sending to server 
			   pwrite.flush(); // flush the data 
			   
			  
			   
				   
		   }
		 /* while(true){
			   pwrite.println("get"); // sending to server 
			   pwrite.flush(); // flush the data
		   }
		 */  
		   
	   }
	   
	   static  class SocketInputReader extends Thread{
		   private InputStream iStream;
		   private String inputCommand="";
		   public SocketInputReader(InputStream iStream){
			   this.iStream = iStream;
		   }
		public void setInputCommand(String inputCommand) {
			this.inputCommand = inputCommand;
		}
		   @Override
	        public void run() {
			   BufferedReader receiveRead = new BufferedReader(new InputStreamReader(iStream));   
			   String receiveMessage;
			   while(true){
				   if(inputCommand.equalsIgnoreCase("quit")){
						  System.out.println("Quitting");
						  try{
						  receiveRead.close();
						  }catch(IOException ioe){
							   ioe.printStackTrace();
						   } finally{
						  break;
						   }
					 }
				   try{
					   if((receiveMessage = receiveRead.readLine()) != null) //receive from server 
					   {
						   
						   	System.out.println(receiveMessage); 
						   	if(inputCommand.equalsIgnoreCase("get")){
						   		System.out.println("received bmr-authenticating vs server");
						   		authorizeFile(receiveMessage);
						   	}
					   	}// displaying at DOS prompt 
				   }
			   catch(Exception e){
				   e.printStackTrace();
			   }
			   }	   
		   }
			   
		   }
	
	
	public  static void authorizeFile( String biometrics){
		boolean valid = testAuth(uid, biometrics);
		if(valid){
			try{
			postFile(filePath);
			System.out.println("File authorized");
			}catch(Exception e){
				System.out.println("Unable to update file");
			}
		}
		
	}
}