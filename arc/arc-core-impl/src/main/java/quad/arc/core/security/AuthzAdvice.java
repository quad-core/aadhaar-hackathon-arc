package quad.arc.core.security;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quad.arc.core.annotation.RequiredPermissions;
import quad.arc.core.annotation.RequiredUser;
import quad.arc.core.util.LangUtil;

@Aspect
public class AuthzAdvice {
    private static final Logger logger = LoggerFactory.getLogger(AuthzAdvice.class);

    @Pointcut("execution(* *(..)) && @annotation(requiredUser) && @within(quad.arc.core.annotation.Api)")
    public void checkUser(RequiredUser requiredUser) {
    }
    
    @Around(value = "checkUser(requiredUser)", argNames = "joinPoint,requiredUser")
    public Object aroundMethodRestrictedForValidUsers(ProceedingJoinPoint joinPoint, RequiredUser requiredUser) throws Throwable {
        try {
            String expectedLoginName = requiredUser.value();
            if (LangUtil.isEmpty(expectedLoginName)) {
                ArcSecurityUtils.assertAuthenticated();
            } else {
                ArcSecurityUtils.assertUser(expectedLoginName);
            }
        } catch (SecurityException e) {
            logger.error("Authorization failed",e);
            throw e;
        }
        return joinPoint.proceed(joinPoint.getArgs());
    }

    @Pointcut("execution(* *(..)) && @annotation(requiredPermissions) && @within(quad.arc.core.annotation.Api)")
    public void authorizedMethods(RequiredPermissions requiredPermissions) {
    }
    
    @Around(value = "authorizedMethods(requiredPermissions)", argNames = "joinPoint,requiredPermissions")
    public Object aroundAuthorizedMethods(ProceedingJoinPoint joinPoint, RequiredPermissions requiredPermissions) throws Throwable {
        
        if (!ArcSecurityUtils.hasPermissions(requiredPermissions.value())) {
            logger.error("Authorization failed.");
            throw new SecurityException("Access denied");
        }
        
        return joinPoint.proceed(joinPoint.getArgs());
    }
    
}
