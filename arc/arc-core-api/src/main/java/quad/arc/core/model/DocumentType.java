package quad.arc.core.model;

public enum DocumentType {
    PASSPORT, DRIVERS_LICENCE, RATION_CARD, VOTER_ID, X_MARKSHEET, BIRTH_CERTIFICATE
}