package quad.arc.core.util;

public interface Entity extends Auditable {
    Long getId();
    
    void setId(Long id);
}
