package quad.arc.core.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import quad.arc.core.util.AbstractEntity;

@Entity
@Table(name="ARC_CITIZEN", 
       uniqueConstraints={@UniqueConstraint(name="UNIQUE_CONSTRAINT",
                                            columnNames={"AADHAAR_NO", "IS_TRASHED", "UPDATED_ON"})})
public class CitizenEntity extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Long id;
	
    private String name;

    private String aadhaarNo;
    
    private String mobileNumber;
    
    private String emailAddress;
    
    private AccountEntity account;
    
    private List<SignatureEntity> signatures;
    
    private List<DocumentEntity> submittedDocuments;
    
    private List<DocumentRequestEntity> receivedDocumentRequests;

	public CitizenEntity() {
		super();
	}

    @Column(name="ARC_CITIZEN_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="CITIZEN_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="AADHAAR_NO")
    public String getAadhaarNo() {
        return aadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        this.aadhaarNo = aadhaarNo;
    }

    @Column(name="MOBILE_NO")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Column(name="EMAIL_ADDRESS")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ARC_ACCOUNT_ID", nullable=false)
    @OnDelete(action=OnDeleteAction.CASCADE)
    @PrimaryKeyJoinColumn
    public AccountEntity getAccount() {
        return account;
    }

    public void setAccount(AccountEntity account) {
        this.account = account;
    }

    @OneToMany(mappedBy="citizen", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @OnDelete(action=OnDeleteAction.CASCADE)
    public List<SignatureEntity> getSignatures() {
        return signatures;
    }

    public void setSignatures(List<SignatureEntity> signatures) {
        this.signatures = signatures;
    }

    @OneToMany(mappedBy="owner", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @OnDelete(action=OnDeleteAction.CASCADE)
    public List<DocumentEntity> getSubmittedDocuments() {
        return submittedDocuments;
    }

    public void setSubmittedDocuments(List<DocumentEntity> submittedDocuments) {
        this.submittedDocuments = submittedDocuments;
    }

    @OneToMany(mappedBy="submittedTo", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @OnDelete(action=OnDeleteAction.CASCADE)
    public List<DocumentRequestEntity> getReceivedDocumentRequests() {
        return receivedDocumentRequests;
    }

    public void setReceivedDocumentRequests(
            List<DocumentRequestEntity> receivedDocumentRequests) {
        this.receivedDocumentRequests = receivedDocumentRequests;
    }
}
