var role = "citizen";
var checkLoggedIn = function() {
	if (role) {
		window.location.hash = '!home';
	} else {
		window.location.hash = '!login';
	}
};
Router = can.Control({
	"route" : function() {
	},
	"home route" : function() {
		if (role == "citizen") {
			var renderer = can.view("templates/citizen.html");
			var frag = renderer({
				certificates : [ 
				                 {id:1, name:"C1"}, 
				                 {id:2, name:"C2"},
				                 {id:3, name:"C3"},
				                 {id:4, name:"C4"},
				                 {id:5, name:"C5"}
				                 ]
			});
		} else {
			var renderer = can.view("templates/admin.html");
			var frag = renderer({
				certificates : [ "a", "b", "c", "d" ]
			});
		}
		$('#view').html(frag);
		$('body').removeClass('login');
	},
	"register route" : function() {
		var renderer = can.view("templates/register.html");
		$('#view').html(renderer);
		$('body').addClass('login');
	},
	"login route" : function() {
		var renderer = can.view("templates/login.html");
		var frag = renderer({
			name : "Justin",
			last : "Meyer"
		}, {
			fullName : function(first, last) {
				return first + " " + last
			}
		});
		$('#view').html(frag);
		$('body').addClass('login');
	},
	"merchant route" : function() {
		var renderer = can.view("templates/merchant/list.html");
		$.ajax({
			url : "http://localhost/learnings/json/he/getAllMerchants.php",
			type : "POST",
			dataType : 'json',
			success : function(data, status, jqXHR) {
				var frag = renderer({
					merchants:data
				});
				$('#view').html(frag);
				$('body').removeClass('login');
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert('error');
			}
		});
	}
});
new Router(document);
can.route.ready();

$('#signIn').on('click', function(e) {
	if (document.loginForm.username.value.length == 0) {
		document.loginForm.username.focus();
		alert('Please enter the username');
	} else if (document.loginForm.password.value.length == 0) {
		document.loginForm.password.focus();
		alert('Please enter the password');
	} else {
		$.ajax({
			url : "http://localhost/learnings/json/he/login.php",
			type : "POST",
			dataType : 'json',
			data : $('#loginForm').serialize(),
			success : function(data, status, jqXHR) {
				if (data.status == "success") {
					role = data.role;
					document.loginForm.reset();
					window.location.hash = '!home';
				} else {
					alert(data.error);
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert('error');
			}
		});
	}
	e.preventDefault();
});
$('#btnRegister').on('click', function(e) {
	if (document.registerForm.aadharNo.value.length == 0) {
		document.registerForm.aadharNo.focus();
		alert('Please enter the aadhar Number');
	} else if (document.registerForm.mobileNo.value.length == 0) {
		document.registerForm.mobileNo.focus();
		alert('Please enter the mobile number');
	} else if (document.registerForm.username.value.length == 0) {
		document.registerForm.username.focus();
		alert('Please enter the username');
	} else {
		$.ajax({
			url : "http://localhost/learnings/json/he/register.php",
			type : "POST",
			dataType : 'json',
			data : $('#registerForm').serialize(),
			success : function(data, status, jqXHR) {
				if (data.status == "success") {
					alert('Registered Successfully');
					document.registerForm.reset();
				} else {
					alert(data.error);
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert('error');
			}
		});
	}
	e.preventDefault();
});
function addMerchant(){
	if (document.merchantForm.name.value.length == 0) {
		document.merchantForm.name.focus();
		alert('Please enter the name');
	} else if (document.merchantForm.username.value.length == 0) {
		document.merchantForm.username.focus();
		alert('Please enter the username');
	} else if (document.merchantForm.email.value.length == 0) {
		document.merchantForm.email.focus();
		alert('Please enter the email address');
	} else {
		$.ajax({
			url : "http://localhost/learnings/json/he/addMerchant.php",
			type : "POST",
			dataType : 'json',
			data : $('#merchantForm').serialize(),
			success : function(data, status, jqXHR) {
				if (data.status == "success") {
					alert('Merchant added Successfully');
					document.merchantForm.reset();
					$('#addMerchant').modal('hide')
				} else {
					alert(data.error);
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert('error');
			}
		});
	}
}
function updateMerchant(){
	if (document.merchantForm.name.value.length == 0) {
		document.merchantForm.name.focus();
		alert('Please enter the name');
	} else if (document.merchantForm.username.value.length == 0) {
		document.merchantForm.username.focus();
		alert('Please enter the username');
	} else if (document.merchantForm.email.value.length == 0) {
		document.merchantForm.email.focus();
		alert('Please enter the email address');
	} else {
		$.ajax({
			url : "http://localhost/learnings/json/he/updateMerchant.php",
			type : "POST",
			dataType : 'json',
			data : $('#merchantForm').serialize(),
			success : function(data, status, jqXHR) {
				if (data.status == "success") {
					alert('Merchant updated Successfully');
					document.merchantForm.reset();
					$('#addMerchant').modal('hide')
				} else {
					alert(data.error);
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert('error');
			}
		});
	}
}

function fnDeleteMerchant(id, name){
	if(confirm("are you sure you want to delete the Merchant("+name+")?")){
		$.ajax({
			url : "http://localhost/learnings/json/he/deleteMerchant.php",
			type : "POST",
			dataType : 'json',
			data : {'id':id},
			success : function(data, status, jqXHR) {
				if (data.status == "success") {
					alert('Deleted Successfully');
				} else {
					alert(data.error);
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert('error');
			}
		});
	}
}
function fnEditMerchant(id){
	$.ajax({
		url : "http://localhost/learnings/json/he/getMerchantById.php", 
		type:'post',
		dataType : 'json',
		data:{'id':id},
		success : function(data, status, jqXHR) {
			if (data.status == "success") {
				document.merchantForm.id.value = data.merchant.id;
				document.merchantForm.name.value = data.merchant.name;
				document.merchantForm.username.value = data.merchant.username; 
				document.merchantForm.email.value = data.merchant.email;
				$('#mode').html('edit');
				$('#addMerchant').modal();
			} else {
				alert(data.error);
			}
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert('error');
		}
	});
}