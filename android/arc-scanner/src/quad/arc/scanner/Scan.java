package quad.arc.scanner;


import java.util.ArrayList;

import com.morpho.capture.AuthBfdCap;
import com.morpho.capture.Errors;
import com.morpho.capture.MorphoTabletFPSensorDevice;
import com.morpho.nfiqscore.Nfiq;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Scan extends Activity implements AuthBfdCap,FingerPrintScanner {
	MorphoTabletFPSensorDevice fpSensorDevice;
	ImageView imageView,imageView2,imageView9;
	TextView requester;
	Button btnnavigate;
	Intent intent;
	int nfiqvalue = 0;
	Nfiq nfiq = new Nfiq();
	List<byte[]> capturedFingers = new ArrayList<byte[]>();
	byte[] data = null;
	byte[] image = null;
	int counter = 0;
	String getSerialNumber=null;
	Bitmap finalBitmap;
	ScannerCallback callback;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scan);
		imageView = (ImageView) findViewById(R.id.imageView1);
		imageView2 = (ImageView) findViewById(R.id.imageView1);
		requester = (TextView) findViewById(R.id.textView1);
		fpSensorDevice = new MorphoTabletFPSensorDevice(this);
		fpSensorDevice.open(this);	
		
		//Server connect code//
		final Button button = (Button) findViewById(R.id.btnStart);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView textView = (TextView)findViewById(R.id.textView2);
                try {
                    WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
                    int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
                    final String formatedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
                            (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));

                    Log.i("MAIN", "Starting control center");
                    String address = ControlCenter.startup(formatedIpAddress,Scan.this);
                    textView.setText("Bound:" +address);
                } catch (Exception e) {
                    Log.e("MAIN", "Server startup failed: " + e.getMessage());
                    textView.setText(e.getMessage());
                }
            }
        });
		
		
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    
        WifiManager wifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();
        final String formatedIpAddress = String.format("%d.%d.%d.%d", (ipAddress & 0xff), (ipAddress >> 8 & 0xff),
                (ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff));
        
        TextView textView = (TextView)findViewById(R.id.textView2);
        textView.setText(formatedIpAddress);     
	}
	
	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		
		fpSensorDevice = new MorphoTabletFPSensorDevice(this);
		fpSensorDevice.open(this);	
	}
	
	
	public void getDeviceDetails(View view){
		String toastOutput ="";
		boolean deviceStatus = fpSensorDevice.isDeviceConnected();
		toastOutput = "deviceStatus:  " + deviceStatus;
		if(deviceStatus){
			String getSerialNumber = fpSensorDevice.getSerialNumber();
			String deviceMake = fpSensorDevice.getDeviceMake();
			String deviceModel = fpSensorDevice.getDeviceModel();
			toastOutput += "+ serialNumber:"+ getSerialNumber +"+ deviceMake:"+deviceMake+"+ deviceModel:"+deviceModel;
			Toast.makeText(getApplicationContext(),
					toastOutput,
					Toast.LENGTH_LONG).show();
			
		}else{
			Toast.makeText(getApplicationContext(),
					toastOutput,
					Toast.LENGTH_SHORT).show();
		}
	}
	public void isDeviceConnected(View view)
	{
		boolean deviceStatus = fpSensorDevice.isDeviceConnected();

		Toast.makeText(getApplicationContext(),
				"deviceStatus:  " + deviceStatus,
				Toast.LENGTH_SHORT).show();
	}

	
	private boolean isDeviceConnected() {
		boolean deviceStatus = fpSensorDevice.isDeviceConnected();

		if(!deviceStatus)
		Toast.makeText(getApplicationContext(),
				"deviceStatus:  " + deviceStatus,
				Toast.LENGTH_SHORT).show();
		return deviceStatus;
	}
	/**
	 * 
	 * @param view
	 *            initializing finger print capture
	 */

	public void captureFinger(View view) {
//		fpSensorDevice.open(this);
		if(isDeviceConnected()){
		fpSensorDevice.setViewToUpdate(imageView);
		try {
			fpSensorDevice.startCapture();
			
		} catch (Exception e) {

			e.printStackTrace();
		}
		}
	}
		
	
	public void getSerialNumber(View view) {
		if(isDeviceConnected()){
		String getSerialNumber = fpSensorDevice.getSerialNumber();
		Toast.makeText(getApplicationContext(),
				"Serial Number: " + getSerialNumber,
				Toast.LENGTH_SHORT).show();
	
		}
		}
	
	
	public void getDeviceMake(View view) {
		if(isDeviceConnected()){
		String deviceMake = fpSensorDevice.getDeviceMake();
		Toast.makeText(getApplicationContext(),
				"Device Make: " + deviceMake,
				Toast.LENGTH_SHORT).show(); 
	}
	}
	
	
	public void getDeviceModel(View view) {
		if(isDeviceConnected()){
		String deviceModel = fpSensorDevice.getDeviceModel();
		Toast.makeText(getApplicationContext(),
				"Device Model: " + deviceModel,
				Toast.LENGTH_SHORT).show();
		}
	}
	
	
	public void getNfiq(View view) {
		if(isDeviceConnected()){
		nfiqvalue = nfiq.getNfiq(fpSensorDevice.lastImage,
				256, 400, 8, 500);
		Toast.makeText(getApplicationContext(),
				"NFIQ: " + nfiqvalue,
				Toast.LENGTH_SHORT).show();
		}
	}
	
	
	public void getCapturedTemplate(View view) {
	
		if(isDeviceConnected()){
		String encodedTemplate = null;
		encodedTemplate = com.example.testproject.Base64.encode(data);
		Toast.makeText(getApplicationContext(),
				"EncodedTemplate: " + encodedTemplate,
				Toast.LENGTH_SHORT).show();		
	
		}
		}
	
	
	public void getCapturedImage(View view) {
		if(isDeviceConnected()){
		imageView2.setImageBitmap(finalBitmap);
		}
	}

	/**
	 * 
	 * @param templateList
	 * @return Finger verification method
	 */

	public void compareTemplates(View view) {
		if(isDeviceConnected()){
		capturedFingers.add(data);		
		try {
			for (byte[] bs : capturedFingers) {
				Errors e = fpSensorDevice.verifyMatch(bs, data);
				if (e != Errors.MATCH_FAILED) {
					if (e == Errors.SUCESS) {
						Toast.makeText(getApplicationContext(),
								"Fingers Matched!!",Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(getApplicationContext(),
								"Fingers not Matched!!",Toast.LENGTH_SHORT).show();						
					}
				}
			}
		} catch (Exception Ex) {
			Ex.printStackTrace();
		}
		}
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.scan, menu);
		return true;
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	@Override
	public void updateImageView(final ImageView im, final Bitmap bm,
			final String mesg, boolean arg3, int arg4) {

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				

				if (im != null) {
					im.setImageBitmap(bm);
				}

				if (mesg.equalsIgnoreCase("Finger captured successfully")) {
					try {
                        
						Toast.makeText(getApplicationContext(),
								"Finger captured successfully",
								Toast.LENGTH_SHORT).show();
						
						//getSerialNumber = fpSensorDevice.getSerialNumber();
						//for fetching template
						data = fpSensorDevice.templateBuffer;
						String encodedTemplate = com.example.testproject.Base64.encode(data);
						if (callback!=null) {
						callback.onCapture(encodedTemplate);
//						Toast.makeText(getApplicationContext(),
//								"EncodedTemplate: " + encodedTemplate,
//								Toast.LENGTH_SHORT).show();	
//						//for fetching RAW Image
						requester.setText("Data Sent to  "+ callback.getHostName());
						}
                        image = fpSensorDevice.lastImage;
                        
                        //convert RAW to Bitmap
                        finalBitmap = fpSensorDevice.convertRAWtoBitmap(image);                       
                        

					} catch (Exception e) {
						if (callback!=null) {
							callback.onCapture("Error occured "+e.getMessage());
//							Toast.makeText(getApplicationContext(),
//									"EncodedTemplate: " + encodedTemplate,
//									Toast.LENGTH_SHORT).show();	
//							//for fetching RAW Image
							requester.setText("Error occurred while sending to "+ callback.getHostName());
							}
						e.printStackTrace();
					}

				}
			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (fpSensorDevice != null) {
			fpSensorDevice.cancelLiveAcquisition();
			fpSensorDevice.release();
			fpSensorDevice = null;
		}
	}

	@Override
	public void setQlyFinger(int qly) {

	}
	
	
	
	@Override
	public void capture(ScannerCallback callback) {
		this.callback=callback;
		final String hostName = callback.getHostName();
		runOnUiThread(new Runnable() {	
				@Override
				public void run() {
					requester.setText("Scan request from "+ hostName);
					if(isDeviceConnected()){
						fpSensorDevice.setViewToUpdate(imageView);
						try {
							fpSensorDevice.startCapture();
							
						} catch (Exception e) {
							requester.setText("Error occured while enabling scanner");
							
							e.printStackTrace();
						}
						
					}else{
						requester.setText("Device not connected");
					}
				}
				}
			);
		
	}

	@Override
	public void showMessage(String message) {
		requester.setText(message);
		
	}

	
	
}
