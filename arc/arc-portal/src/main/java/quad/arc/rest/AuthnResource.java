package quad.arc.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;

import quad.arc.core.model.Account;
import quad.arc.core.security.ArcSecurityUtils;
import quad.arc.core.security.Authenticator;

@Path("/authn")
@Produces("application/json")
@Consumes("application/json")
public class AuthnResource {
    
    @Autowired
    private Authenticator authenticator;
    
    @GET
    public Account getContextUserInfo() {
        return ArcSecurityUtils.getContextUserInfo();
    }
}
