package quad.arc.core.service;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import quad.arc.core.model.Citizen;

@Transactional(propagation = Propagation.REQUIRED)
public interface RegistrationService {
    public void registerCitizen(Citizen citizen);
}