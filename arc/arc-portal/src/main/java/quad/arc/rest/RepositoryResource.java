package quad.arc.rest;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.FileSizeLimitExceededException;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
@Path("/ext/repository")
@Produces("application/json")
public class RepositoryResource {

	public RepositoryResource() {
		super();
	}
	
    @POST
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Response upload(@Context HttpServletRequest 	 request){
    	
    		if (!ServletFileUpload.isMultipartContent(request))
            {
    			System.out.println("Not Multipart");
    			return Response.serverError().status(Status.BAD_REQUEST).build();
            }
    		DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(100 * 1024);
            factory.setRepository(new File("/Users/jkakka/tmpfilestore/"));
            ServletFileUpload servletFileUpload = new ServletFileUpload(factory);
            servletFileUpload.setSizeMax(10000000l);
            try{
	            List<FileItem> parsedList = servletFileUpload.parseRequest(request);
	            if(CollectionUtils.isEmpty(parsedList)){
	            	System.out.println("Empty Request");
	            	return Response.serverError().status(Status.BAD_REQUEST).build();
	    		    
	            }
	            int len;
	            byte[] buffer = new byte[1024*100]; //100KB
	            FileOutputStream fileOutStream;
	            BufferedInputStream buffInput = null;
	            BufferedOutputStream buffOut = null;
	            try {
	                boolean overallStatus = true;
	                for(FileItem item : parsedList){
	                    String partFileName = item.getName();
	                    if(partFileName == null)
	                    	continue;
	                    System.out.println("partfileNAME="+partFileName);
	                    File file = new File("/Users/jkakka/tmpfilestore/", partFileName);
	                    try {
	                        if(!file.createNewFile()) {
	                            System.out.println("File already exists");
	                        } 
	                    } catch (IOException ex) {
	                        System.out.println(ex);
	                    }
	                    //file.createNewFile();
	                    try
	                    {
	                        fileOutStream = new FileOutputStream(file);
	                        buffInput = new BufferedInputStream(item.getInputStream());
	                        buffOut = new BufferedOutputStream(fileOutStream);
	                        while ((len=buffInput.read(buffer))>0)
	                        {
	                            buffOut.write(buffer, 0, len);
	                        }
	                    }
	                    finally
	                    {
	                        // Closing the stream
	                        closeStreamQuietLy(buffInput);
	                        closeStreamQuietLy(buffOut);
	                    }
	                    //File renamedFile = new File(tempFolder,item.getFileName());
	                    
	                    //boolean isFileRenamed = file.renameTo(renamedFile);
	                    
	                      
	                }
	                return Response.ok().status(Status.CREATED).build();
	            } catch (Exception e) {
	            	e.printStackTrace();
	            	return Response.serverError().status(Status.BAD_REQUEST).build();
	    		    
	            }finally {
	               closeStreamQuietLy(buffInput);
	               closeStreamQuietLy(buffOut);
	            }
	            
	        }catch(Exception e){
	        	System.out.println("Some error occured");
	        	e.printStackTrace();
            	return Response.serverError().status(Status.BAD_REQUEST).build();
    		    
            }
            

	}

	public static void closeStreamQuietLy(Closeable in)
	{
	    if (in != null)
	    {
	        try
	        {
	            in.close();
	        }
	        catch (IOException ignore)
	        {
	            //ignore
	        }
	    }
	}
}

