app
		.controller(
				"HomeController",
				function($scope, $rootScope, CookieService, DocumentService,
						DTOptionsBuilder, DTColumnBuilder, $compile, ngDialog, fileUpload, $location) {
					$scope.showHeader(true);
					function callback(json) {
						console.log(json);
					};
					if (CookieService.isAdmin()) {
						//$scope.template = 'templates/dashboard/admin.html';
						$location.path('merchants');
					} else if (CookieService.isCitizen()) {
						$scope.template = 'templates/dashboard/citizen.html';
						$scope.FormData ={ myFile :  ''};
						$scope.uploadFile = function(){
					        var file = $scope.FormData.myFile;
					        var uploadUrl = "http://localhost/learnings/json/he/uploadCertificate.php";
					        var def = fileUpload.uploadFileToUrl(file, uploadUrl);
					        def.then(function(response){
					        	alert('Certificate uploaded successfully');
					        	$scope.certificateOptions.dtInstance.reloadData();
					        	$scope.modal.close();
					        }, function(error){
					        	alert(error);
					        });
					    };
						$scope.fnUploadCertificate = function() {
							$scope.modal = ngDialog
									.open({
										template : 'templates/dashboard/uploadCertificate.html',
										scope : $scope,
										className : 'ngdialog-theme-plain'
									});
						};
						$scope.fnMakeDefault = function(id){
							var certDef = DocumentService.makeCertificateDefault(id);
							certDef.then(function(response){
								$scope.certificateOptions.dtInstance.reloadData();
							}, function(error){
								alert(error);
							});
						};
						$scope.documentTypes = {
							'passport':'PASSPORT', 
							'Driver License': 'DRIVERS_LICENCE', 
							'Ration card':'RATION_CARD', 
							'Voter Id' : 'VOTER_ID', 
							'X Marksheet':'X_MARKSHEET', 
							'Birth Certificate':'BIRTH_CERTIFICATE'	
						};
						$scope.documentToBeShared = {};
						$scope.fnShareSignDoc = function(id, file){
							$scope.documentToBeShared.id = id;
							$scope.documentToBeShared.file = file;
							$scope.documentToBeShared.types = [];
							$scope.shareDocDialog = ngDialog.open({
								template: 'sharedDocDialog',
								className : 'ngdialog-theme-plain custom-width',
								scope:$scope
							});
						};
						$scope.fnSelectDocTypes = function(value){
							var index = $scope.documentToBeShared.types.indexOf(value); 
							if(index==-1){
								$scope.documentToBeShared.types.push(value);
							}
							else{
								$scope.documentToBeShared.types.splice(index,1);
							}
						};
						$scope.shareSignDoc = function(){
							var def = DocumentService.shareDocById($scope.documentToBeShared.id, $scope.documentToBeShared.types);
							def.then(function(response){
								alert('Document Shared successfully');
								$scope.shareDocDialog.close();
								$scope.sharedDocOptions.dtInstance.reloadData();
							}, function(error){
								alert(error);
							});
						};
						$scope.fnUnshareDoc = function(id){
							var def = DocumentService.unshareDocById(id);
							def.then(function(response){
								alert('Document unshared successfully');
								$scope.sharedDocOptions.dtInstance.reloadData();
							}, function(error){
								alert(error);
							});
						};
						/***************** START-------Certification datatable ************/
						$scope.certificateOptions = {};
						$scope.certificateOptions.dtOptions = DTOptionsBuilder
								.fromSource(
										'http://localhost/learnings/json/he/getAllMyCertificates.php')
								.withOption(
										'fnRowCallback',
										function(nRow, aData, iDisplayIndex,
												iDisplayIndexFull) {
											$compile(nRow)($scope);
										})
								.withOption(
										'fnDrawCallback',
										function(oSettings) {
											angular
													.element(
															'#certificate-toolbar')
													.addClass(
															'toolbar pull-left');
											angular
													.element(
															'#certificate-toolbar')
													.html(
															'<div class="btn-group" role="group">'
																	+ '<button type="button" class="btn btn-success" ng-click="fnUploadCertificate()"><i class="fa fa-upload"></i> Upload</button>'
																	+ '<button type="button" class="btn btn-info" ng-click="certificateOptions.reloadData()"><i class="fa fa-refresh"></i> Refresh</button>'
																	+ '</div>');
											$compile(
													angular
															.element(
																	'#certificate-toolbar')
															.contents())
													($scope);
										}).withOption('dom',
										'<"#certificate-toolbar">frt')
								.withPaginationType('full_numbers');
						$scope.certificateOptions.dtColumns = [
								DTColumnBuilder.newColumn('file').withTitle(
										'File'),
								DTColumnBuilder.newColumn('createdDate').withTitle(
										'Created Date'),
								DTColumnBuilder
										.newColumn('id')
										.withTitle('')
										.notSortable()
										.renderWith(
												function(data, type, full) {
													var button = '';
													if(full.default){
														button = '<i class="fa fa-dot-circle-o text-success"></i>';
													}
													else{
														button = '<a href="javascript:void(0)" title="Make Default" ng-click="fnMakeDefault('+data+')"><i class="fa fa-circle-o text-muted"></i></a>';
													}
													return button;
												}) ];
						$scope.certificateOptions.dtInstance = {};
						$scope.certificateOptions.reloadData = function(){
							var resetPaging = false;
							$scope.certificateOptions.dtInstance.reloadData(callback, resetPaging);
						}
						$scope.certificateOptions.dtInstanceCallback = function(dtInstance) {
							$scope.certificateOptions.dtInstance = dtInstance;
						};
						/**************** END-------Certification datatable ************/					
						
						/**************** START-----Signed Documents datatable *******/
						$scope.signedDocOptions = {};
						$scope.signedDocOptions.dtOptions = DTOptionsBuilder
								.fromSource(
										'http://localhost/learnings/json/he/getAllSignedDocuments.php')
								.withOption(
										'fnRowCallback',
										function(nRow, aData, iDisplayIndex,
												iDisplayIndexFull) {
											$compile(nRow)($scope);
										})
								.withOption(
										'fnDrawCallback',
										function(oSettings) {
											angular
													.element(
															'#signdoc-toolbar')
													.addClass(
															'toolbar pull-left');
											angular
													.element(
															'#signdoc-toolbar')
													.html(
															'<button type="button" class="btn btn-info" ng-click="signedDocOptions.reloadData()"><i class="fa fa-refresh"></i> Refresh</button>');
											$compile(
													angular
															.element(
																	'#signdoc-toolbar')
															.contents())
													($scope);
										}).withOption('dom',
										'<"#signdoc-toolbar">frtip')
								.withPaginationType('simple_numbers');
						$scope.signedDocOptions.dtColumns = [
								DTColumnBuilder.newColumn('file').withTitle(
										'File'),
								DTColumnBuilder.newColumn('tags').withTitle(
										'Tags'),
								DTColumnBuilder.newColumn('merchant').withTitle(
										'Merchant'),
								DTColumnBuilder.newColumn('createdDate').withTitle(
										'Created Date'),
								DTColumnBuilder
										.newColumn('id')
										.withTitle('')
										.notSortable()
										.renderWith(
												function(data, type, full) {
													var button = '<a href="javascript:void(0)" title="Share Document" ng-click="fnShareSignDoc('+data+', \''+full.file+'\')"><i class="fa fa-share-alt text-primary"></i></a>';
													return button;
												}) ];
						$scope.signedDocOptions.dtInstance = {};
						$scope.signedDocOptions.reloadData = function(){
							var resetPaging = true;
							$scope.signedDocOptions.dtInstance.reloadData(callback, resetPaging);
						}
						$scope.signedDocOptions.dtInstanceCallback = function(dtInstance) {
							$scope.signedDocOptions.dtInstance = dtInstance;
						};
						/**************** END-----Signed Documents datatable *******/
						
						/**************** START-----Shared Documents datatable *******/
						$scope.sharedDocOptions = {};
						$scope.sharedDocOptions.dtOptions = DTOptionsBuilder
								.fromSource(
										'http://localhost/learnings/json/he/getAllSharedDocuments.php')
								.withOption(
										'fnRowCallback',
										function(nRow, aData, iDisplayIndex,
												iDisplayIndexFull) {
											$compile(nRow)($scope);
										})
								.withOption(
										'fnDrawCallback',
										function(oSettings) {
											angular
													.element(
															'#shareddoc-toolbar')
													.addClass(
															'toolbar pull-left');
											angular
													.element(
															'#shareddoc-toolbar')
													.html(
															'<button type="button" class="btn btn-info" ng-click="sharedDocOptions.reloadData()"><i class="fa fa-refresh"></i> Refresh</button>');
											$compile(
													angular
															.element(
																	'#shareddoc-toolbar')
															.contents())
													($scope);
										}).withOption('dom',
										'<"#shareddoc-toolbar">frtip')
								.withPaginationType('simple_numbers');
						$scope.sharedDocOptions.dtColumns = [
								DTColumnBuilder.newColumn('file').withTitle(
										'File'),
								DTColumnBuilder.newColumn('type').withTitle(
										'Type'),
								DTColumnBuilder
										.newColumn('id')
										.withTitle('')
										.notSortable()
										.renderWith(
												function(data, type, full) {
													var button = '<a href="javascript:void(0)" title="Unshare Document" ng-click="fnUnshareDoc('+data+')"><i class="fa fa-times text-danger"></i></a>';
													return button;
												}) ];
						$scope.sharedDocOptions.dtInstance = {};
						$scope.sharedDocOptions.reloadData = function(){
							var resetPaging = true;
							$scope.sharedDocOptions.dtInstance.reloadData(callback, resetPaging);
						}
						$scope.sharedDocOptions.dtInstanceCallback = function(dtInstance) {
							$scope.sharedDocOptions.dtInstance = dtInstance;
						};
						/**************** END-----Shared Documents datatable *******/
					} else if (CookieService.isMerchant()) {
						$scope.template = 'templates/dashboard/merchant.html';
						$scope.fnViewDocList = function(id){
							$scope.docDetails = {};
							var def = DocumentService.getDocumentSharedWithMeById(id);
							def.then(function(response){
								$scope.docDetails = response.docDetails;
								$scope.viewDocDialog = ngDialog.open({
									template:'docSharedDialog',
									className : 'ngdialog-theme-plain custom-width',
									scope:$scope
								});
							}, function(error){
								alert(error);
							})
						};
						/**************** START-----Request Form *******/
						$scope.request = {
							documents:[]
						};
						$scope.documentList = {
								'passport':'PASSPORT', 
								'Driver License': 'DRIVERS_LICENCE', 
								'Ration card':'RATION_CARD', 
								'Voter Id' : 'VOTER_ID', 
								'X Marksheet':'X_MARKSHEET', 
								'Birth Certificate':'BIRTH_CERTIFICATE'	
						};
						$scope.docSelected = function(value){
							var index = $scope.request.documents.indexOf(value); 
							if(index==-1){
								$scope.request.documents.push(value);
							}
							else{
								$scope.request.documents.splice(index,1);
							}
						};
						$scope.sendRequest = function(){
							var def = DocumentService.sendRequest($scope.request);
							def.then(function(response){
								alert('Request sent');
								$scope.request = {
									documents:[]
								};
							}, function(error){
								alert(error);
							});
						};
						/**************** END-----Request Form *******/
						
						/**************** START-----Docs Shared with me datatable *******/
						$scope.docsShared = {};
						$scope.docsShared.dtOptions = DTOptionsBuilder
								.fromSource(
										'http://localhost/learnings/json/he/getAllDocumentsSharedWithMe.php')
								.withOption(
										'fnRowCallback',
										function(nRow, aData, iDisplayIndex,
												iDisplayIndexFull) {
											$compile(nRow)($scope);
										})
								.withOption(
										'fnDrawCallback',
										function(oSettings) {
											angular
													.element(
															'#docsShared-toolbar')
													.addClass(
															'toolbar pull-left');
											angular
													.element(
															'#docsShared-toolbar')
													.html(
															'<button type="button" class="btn btn-info" ng-click="docsShared.reloadData()"><i class="fa fa-refresh"></i> Refresh</button>');
											$compile(
													angular
															.element(
																	'#docsShared-toolbar')
															.contents())
													($scope);
										}).withOption('dom',
										'<"#docsShared-toolbar">frtip')
								.withPaginationType('simple_numbers');
						$scope.docsShared.dtColumns = [
								DTColumnBuilder.newColumn('name').withTitle(
										'Citizen Name'),
								DTColumnBuilder.newColumn('aadharNo').withTitle(
										'Aadhar No'),
								DTColumnBuilder.newColumn('id').withTitle(
										'').notSortable().renderWith(function(data, display, full){
											var button = '<a href="javascript:void(0)" title="View Document List" ng-click="fnViewDocList('+data+')"><i class="fa fa-eye text-primary"></i></a>';
											return button;
										})
						];
						$scope.docsShared.dtInstance = {};
						$scope.docsShared.reloadData = function(){
							var resetPaging = true;
							$scope.docsShared.dtInstance.reloadData(callback, resetPaging);
						}
						$scope.docsShared.dtInstanceCallback = function(dtInstance) {
							$scope.docsShared.dtInstance = dtInstance;
						};
						/**************** END-----Docs Shared with me datatable *******/
						
						/**************** START-----Requests datatable *******/
						$scope.requestsOptions = {};
						$scope.requestsOptions.dtOptions = DTOptionsBuilder
								.fromSource(
										'http://localhost/learnings/json/he/getAllRequests.php')
								.withOption(
										'fnRowCallback',
										function(nRow, aData, iDisplayIndex,
												iDisplayIndexFull) {
											$compile(nRow)($scope);
										})
								.withOption(
										'fnDrawCallback',
										function(oSettings) {
											angular
													.element(
															'#requests-toolbar')
													.addClass(
															'toolbar pull-left');
											angular
													.element(
															'#requests-toolbar')
													.html(
															'<button type="button" class="btn btn-info" ng-click="requestsOptions.reloadData()"><i class="fa fa-refresh"></i> Refresh</button>');
											$compile(
													angular
															.element(
																	'#requests-toolbar')
															.contents())
													($scope);
										}).withOption('dom',
										'<"#requests-toolbar">frtip')
								.withPaginationType('simple_numbers');
						$scope.requestsOptions.dtColumns = [
								DTColumnBuilder.newColumn('name').withTitle(
										'Citizen Name'),
								DTColumnBuilder.newColumn('aadharNo').withTitle(
										'Aadhar No'),
								DTColumnBuilder.newColumn('documentTypeList').withTitle(
										'List of document types'),
								DTColumnBuilder.newColumn('date').withTitle(
										'Date'),
								DTColumnBuilder.newColumn('status').withTitle(
										'Status'),
						];
						$scope.requestsOptions.dtInstance = {};
						$scope.requestsOptions.reloadData = function(){
							var resetPaging = true;
							$scope.requestsOptions.dtInstance.reloadData(callback, resetPaging);
						}
						$scope.requestsOptions.dtInstanceCallback = function(dtInstance) {
							$scope.requestsOptions.dtInstance = dtInstance;
						};
						/**************** END-----Requests datatable *******/
					}
				});