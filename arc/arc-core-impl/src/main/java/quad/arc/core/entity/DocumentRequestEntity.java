package quad.arc.core.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import quad.arc.core.model.CitizenResponseCode;
import quad.arc.core.model.DocumentType;
import quad.arc.core.util.AbstractEntity;

@Entity
@Table(name="ARC_DOC_REQ")
public class DocumentRequestEntity extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Long id;
	
    private Set<DocumentType> documentTypes;
    
    private CitizenResponseCode responseCode;
    
    private CitizenEntity submittedTo;
    
    private MerchantUserEntity merchantUser;
    
	public DocumentRequestEntity() {
		super();
	}

    @Column(name="ARC_ACCOUNT_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
    @Type(type = "quad.arc.core.entity.DocumentTypesUserType")
    @Column(name="DOC_TYPES")
    public Set<DocumentType> getDocumentTypes() {
        return documentTypes;
    }

    public void setDocumentTypes(Set<DocumentType> documentTypes) {
        this.documentTypes = documentTypes;
    }

    @Column(name="RESP_CODE")
    public CitizenResponseCode getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(CitizenResponseCode responseCode) {
        this.responseCode = responseCode;
    }
    
    @ManyToOne
    @JoinColumn(name="ARC_CITIZEN_ID")
    public CitizenEntity getSubmittedTo() {
        return submittedTo;
    }

    public void setSubmittedTo(CitizenEntity submittedTo) {
        this.submittedTo = submittedTo;
    }

    @ManyToOne
    @JoinColumn(name="ARC_MERCHANT_USER_ID")
    public MerchantUserEntity getMerchantUser() {
        return merchantUser;
    }

    public void setMerchantUser(MerchantUserEntity merchantUser) {
        this.merchantUser = merchantUser;
    }
}
