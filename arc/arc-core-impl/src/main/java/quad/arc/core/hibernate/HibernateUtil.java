package quad.arc.core.hibernate;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

final public class HibernateUtil {
	private static final Map<String, String> domainDialectMap = new TreeMap<String, String>();
	static {
    	/*
    	 * Select value for 'hibernate.dialect' based on the datasource class type.
    	 * 
    	 * MySQL                -> org.hibernate.dialect.MySQL5InnoDBDialect
    	 * Microsoft SQL Server -> org.hibernate.dialect.SQLServerDialect
    	 * HSQLDB               -> org.hibernate.dialect.HSQLDialect 
    	 * PostgreSQL           -> org.hibernate.dialect.PostgreSQLDialect
    	 */
		domainDialectMap.put("com.mysql", "org.hibernate.dialect.MySQL5InnoDBDialect");
		domainDialectMap.put("com.microsoft.sqlserver", "org.hibernate.dialect.SQLServerDialect");
		domainDialectMap.put("org.hsqldb", "org.hibernate.dialect.HSQLDialect");
		domainDialectMap.put("org.postgresql", "org.hibernate.dialect.PostgreSQLDialect");
	}
    private HibernateUtil() {
    }
    public static SessionFactory configure() throws Exception {
    	AnnotationConfiguration configuration = new AnnotationConfiguration();

    	InitialContext initialContext = new InitialContext();
    	DataSource dataSource = (DataSource)initialContext.lookup("java:comp/env/jdbc/arc");
    	if (dataSource!=null) {
            
            String dialect = null;
            
            Class<?> clazz = dataSource.getClass();
            String dataSourceType = clazz.getName();
            
            if (dataSourceType.contains("tomcat")) {
                Method method = clazz.getMethod("getDriverClassName");
                dataSourceType = (String)method.invoke(dataSource);
            }
            
            for(Map.Entry<String, String> e:domainDialectMap.entrySet()) {
            	if (dataSourceType.startsWith(e.getKey())) {
            		dialect = e.getValue();
            	}
            }
                	    
            InputStream in = HibernateUtil.class.getResourceAsStream("/hibernate.properties");
            Properties properties = new Properties();
            try {
                properties.load(in);
                in.close();
                
                System.out.println("Properties: " + properties);

                if (dialect!=null) {
                    properties.setProperty("hibernate.dialect", dialect);
                }

                configuration.setProperties(properties);
            } catch(Exception e) {
                 e.printStackTrace();
            }
    	}    	
        
        configuration.configure();
        
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        
        return sessionFactory;
    }
}
