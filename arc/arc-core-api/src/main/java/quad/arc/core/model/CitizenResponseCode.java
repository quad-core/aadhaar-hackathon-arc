package quad.arc.core.model;

public enum CitizenResponseCode {
    ACCEPTED, DECLINED
}
