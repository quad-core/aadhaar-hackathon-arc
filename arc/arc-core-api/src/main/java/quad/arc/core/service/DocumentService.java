package quad.arc.core.service;

import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import quad.arc.core.model.CitizenResponseCode;
import quad.arc.core.model.Document;
import quad.arc.core.model.DocumentRequest;
import quad.arc.core.model.DocumentStoreRequest;
import quad.arc.core.model.DocumentType;

@Transactional(propagation = Propagation.REQUIRED)
public interface DocumentService {
    // document:store
    public void storeDocument(DocumentStoreRequest document);
       
    // document:view
    public List<Document> getAllDocuments();
    
    // document:view
    public List<Document> getSharedDocuments();
    
    // document:share
    public void unshareDocument(long documentId);
    
    // document:share
    public void shareDocument(long documentId, DocumentType documentType);
    
    // document-request:create
    public void requestDocument(String aadhaarNo, Set<DocumentType> documentType);
    
    // document-request:view
    public List<DocumentRequest> getDocumentRequests();
    
    // document-request:respond
    public void submitResponse(long documentRequestId, CitizenResponseCode responseCode);
}