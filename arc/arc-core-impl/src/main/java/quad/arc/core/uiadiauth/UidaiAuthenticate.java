package quad.arc.core.uiadiauth;

import java.io.FileInputStream;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

@SuppressWarnings("deprecation")
public class UidaiAuthenticate {
	// https://ac.khoslalabs.com/hackgate/hackathon/auth/raw
	protected static final String CONTENT_HEADER_ACCEPT = "Accept";
	protected static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
	protected static final String CONTENT_HEADER_TYPE = "Content-Type";
	protected static final int CONNECTION_TIMEOUT = 10000;
	protected static final int SOCKET_TIMEOUT = 60000;
	protected static final String URL = "https://ac.khoslalabs.com/hackgate/hackathon/auth/raw";

	public boolean authenticate(String uid,String rawBiometric) {
		
		String jsonRequest = "{\"aadhaar-id\": \""+uid+"\","
				+ "\"raw-biometrics\": \""+rawBiometric+"\","
				+ "\"modality\": \"biometric\","
				+ "\"modality-type\": \"fp\","
				+ "\"number-of-fingers-to-capture\": 1,"
				+ "\"certificate-type\": \"preprod\","
				+ "\"location\": {\"type\": \"pincode\",\"pincode\": \"560103\"}}";

		HttpParams httpparams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpparams,
				CONNECTION_TIMEOUT);
		HttpConnectionParams.setSoTimeout(httpparams, SOCKET_TIMEOUT);

		HttpClient httpclient = getHttpClient(httpparams);
		HttpPost httppost = new HttpPost(URL);
		httppost.setHeader(CONTENT_HEADER_TYPE, CONTENT_TYPE_APPLICATION_JSON);
		httppost.setHeader(CONTENT_HEADER_ACCEPT, CONTENT_TYPE_APPLICATION_JSON);

		try {
			System.out.println(jsonRequest);
			StringEntity entity = new StringEntity(jsonRequest);
			entity.setContentType(CONTENT_TYPE_APPLICATION_JSON);
			httppost.setEntity(entity);
		} catch (UnsupportedEncodingException e) {
			System.err.println("Error while communicating with the server");

		}

		try {
			HttpResponse response = httpclient.execute(httppost);
			String responseContent = EntityUtils.toString(response.getEntity());
			if (response != null
					&& response.getStatusLine().getStatusCode() == 200) {
				System.out.println("RESPONSE" + responseContent);
				return true;
			} else {
				System.err.println("Failed");
			}
			return false;
					
		} catch (Exception e) {
			e.printStackTrace();
			System.err
					.println("COMMUNICATION_ERROR"
							+ "Error while communicating with the server. Check connectivity.");
			return false;
		}
	}

	private HttpClient getHttpClient(HttpParams params) {
		try {

			//String filename = "/Users/anilchan/git/arc/arc-core-impl/jssecacerts";
			String password = "changeit";

			KeyStore trustStore = KeyStore.getInstance("JKS");
			trustStore.load(ClassLoader.getSystemResourceAsStream("jssecacerts"),
					password.toCharArray());

			SSLSocketFactory sf = new TrustAllCertsSSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

			SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory
					.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));

			ClientConnectionManager ccm = new ThreadSafeClientConnManager(
					params, registry);
			DefaultHttpClient client = new DefaultHttpClient(ccm, params);
			client.setHttpRequestRetryHandler(new DefaultHttpRequestRetryHandler(
					0, false));
			return client;
		} catch (Exception e) {
			return new DefaultHttpClient();
		}
	}

	public static void main(String[] args) {
		
		String uid="630476123627";
		String rawBiometric = "Rk1SACAyMAAAAADMAAABAAGQAMUAxQEAAQAlG0CIABuUAEB6ACQUAEB3AEcQAIBPAFMYAICPAF8MAEAgAG8cAEBUAHKUAEBSAJCUAIDFAJV4AEBdAJcYAEC0AJz4AEAqAKEYAECiAKz4AEC3ALRwAEBEAMmgAICwAOVwAICiAO7oAICwAP9wAEBGAQYkAICPAQagAIDMAQbUAEDAARFcAICBARYkAICRATKsAEAzAT4gAEBzAU8YAEB+AVr8AAAMCgEACGAFXwAAAAAA";
		String jsonRequest = "{\"aadhaar-id\": \""+uid+"\","
				+ "\"raw-biometrics\": \""+rawBiometric+"\","
				+ "\"modality\": \"biometric\","
				+ "\"modality-type\": \"fp\","
				+ "\"number-of-fingers-to-capture\": 1,"
				+ "\"certificate-type\": \"preprod\","
				+ "\"location\": {\"type\": \"pincode\",\"pincode\": \"560103\"}}";

		//System.out.println(jsonRequest);
		new UidaiAuthenticate().authenticate(uid,rawBiometric);
	}

}

class TrustAllCertsSSLSocketFactory extends SSLSocketFactory {
	SSLContext sslContext = SSLContext.getInstance("TLS");

	public TrustAllCertsSSLSocketFactory(KeyStore truststore)
			throws NoSuchAlgorithmException, KeyManagementException,
			KeyStoreException, UnrecoverableKeyException {
		super(truststore);
		TrustManager tm = new X509TrustManager() {
			public void checkClientTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}

			public void checkServerTrusted(X509Certificate[] chain,
					String authType) throws CertificateException {
			}

			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
		};
		sslContext.init(null, new TrustManager[] { tm }, null);
	}
}
