package quad.arc.core.security;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;

import javax.crypto.spec.SecretKeySpec;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import quad.arc.core.model.Account;

final public class ArcSecurityUtils {
    private static final Logger logger = LoggerFactory.getLogger(ArcSecurityUtils.class);
    public static final String ROOT = "root";
    public static final String SYSTEM = "system";
    
    
    private static final byte[] KEY; // FIXME Need to externalize
    private static final AesCipherService cipherService;
    private static final Object salt;
    
    private static final Account SYSTEM_USER_INFO;
    
    static {
        KEY  = (new SecretKeySpec(Base64.decode("dceDN9/L3qa5dZQoIh4NAw=="), "AES")).getEncoded();
        cipherService = new AesCipherService();
//        RandomNumberGenerator rng = new SecureRandomNumberGenerator(); 
        // FIXME Use different salt for each user for better security.
        salt = "dceDN9/L3qa5dZQoIh4NAw==";
        
        SYSTEM_USER_INFO = new Account();
        SYSTEM_USER_INFO.setId(-1L);
        SYSTEM_USER_INFO.setLoginName(SYSTEM);
    }
    
    private static ThreadLocal<Boolean> rootContextThread = new ThreadLocal<Boolean>();
    
    private static ThreadLocal<Boolean> systemContextThread = new ThreadLocal<Boolean>();
    
    private ArcSecurityUtils() {
    }
    
    public static String getSalt() {
        return salt.toString();
    }

    public static void setRootContext() {
        rootContextThread.set(true);
    }
    
    public static void removeRootContext() {
        rootContextThread.set(false);
    }
    
    public static boolean isSystemContext() {
        return systemContextThread.get()!=null&&systemContextThread.get();
    }
    
    public static Account getContextUserInfo() {
        if (isSystemContext()) {
            return SYSTEM_USER_INFO;
        }
        
        Subject subject = SecurityUtils.getSubject();
        
        if (!subject.isAuthenticated()) {
            throw new SecurityException("Unauthorized access");
        }
        
        Account userInfo = (Account)subject.getSession().getAttribute(Account.class.getName());
        return userInfo;
    }
    
    public static boolean isRoot(String loginName) {
        return ROOT.equals(loginName) || SYSTEM.equals(loginName);
    }
    
    public static boolean isRoot() {
        return Boolean.TRUE.equals(rootContextThread.get()) || isRoot(getContextUserName());        
    }
    
    public static String getContextUserName() {
        if (isSystemContext()) {
            return SYSTEM;
        }
        
        Subject subject = SecurityUtils.getSubject();

        String user = (String)subject.getPrincipal();
        
        return user;
    }
    
    public static void assertRoot() {
        if (!isRoot()) {
            throw new SecurityException("Access denied");
        }
    }
    
    public static void assertUser(String user) {
        if (!getContextUserName().equals(user)) {
            throw new SecurityException("Access denied");
        }
    }
    
    public static void assertAuthenticated() {
        if (isSystemContext()) {
            return;
        }

        if (!SecurityUtils.getSubject().isAuthenticated()) {
            throw new SecurityException("Access denied");
        }
    }
    
    public static <A> A runAsSystem(Callable<A> callable) throws Exception {
        systemContextThread.set(true);
        try {
            return callable.call();    
        } finally {
            systemContextThread.set(false);
        }
    }
    
    public static <A> A runAs(Callable<A> callable, String loginName) throws Exception {
        Subject subject = SecurityUtils.getSubject();
        subject.runAs(new SimplePrincipalCollection(loginName, "ArcRealm"));
        A v = callable.call();
        subject.releaseRunAs();
        return v;
    }
    
    public static Set<String> filterPermission(Set<String> permissions) {
        if (isRoot()) {
            return permissions;
        }
        
        Subject subject = SecurityUtils.getSubject();
        
        Set<String> filteredPermissions = new TreeSet<>();
        for(String permission:permissions) {
            if (subject.isPermitted(permission)) {
                filteredPermissions.add(permission);
            }
        }
        
        return filteredPermissions;
    }
    
    public static Set<String> filterPermission(String [] permissions) {
        return filterPermission(new TreeSet<>(Arrays.asList(permissions)));
    }
    
    public static boolean hasPermissions(String [] permissions) {
        return filterPermission(permissions).size() == permissions.length;
    }
    
    public static boolean hasPermission(String permission) {
        Subject subject = SecurityUtils.getSubject();
        
        return subject.isPermitted(permission);
    }
    
    public static void checkPermission(String permission) {
        if (isRoot()) {
            return;
        }

        Subject subject = SecurityUtils.getSubject();
        
        if (!subject.isPermitted(permission)) {
            logger.error("User \"{}\" do not have expected permissions; \"{}\"", subject.getPrincipal(), permission);
            
            throw new SecurityException("Access denied");
        }
    }
    
    public static String hash(String clearText) {
        return new Sha256Hash(clearText, salt).toBase64();
    }
    
    public static String encrypt(String clearText) {
        ByteSource cipher = cipherService.encrypt(clearText.getBytes(), KEY);

        String cipherText = Base64.encodeToString(cipher.getBytes());

        return cipherText;
    }
    
    public static String decrypt(String cipherText) {
        ByteSource clearText = cipherService.decrypt(Base64.decode(cipherText), KEY);
        
        return new String(clearText.getBytes());
    }
}
