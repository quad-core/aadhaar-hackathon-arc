package quad.arc.core.util;

import java.util.Calendar;
import java.util.Date;

import quad.arc.core.security.ArcSecurityUtils;

public class DataUtil {
    public static void clearAuditDetails(AbstractEntity entity) {
        entity.setCreatedBy(null);
        entity.setCreatedOn(null);
        entity.setUpdatedBy(null);
        entity.setUpdatedOn(null);
    }
    
    public static void setAuditDetails(AbstractEntity entity) {
        Date now = Calendar.getInstance().getTime();
        
        String contextUser = ArcSecurityUtils.getContextUserName();
        if (entity.getId()==null) {
            entity.setCreatedBy(contextUser);
            entity.setCreatedOn(now);
        }
        
        entity.setUpdatedBy(contextUser);
    }
    
    public static void copyAuditDetails(Auditable source, Auditable target) {
        target.setCreatedBy(source.getCreatedBy());
        target.setCreatedOn(source.getCreatedOn());
        target.setUpdatedBy(source.getUpdatedBy());
        target.setUpdatedOn(source.getUpdatedOn());
    }
}
