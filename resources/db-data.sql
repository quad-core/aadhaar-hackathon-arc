INSERT INTO arc_account(
            arc_account_id, created_by, created_on, is_trashed, updated_by, 
            updated_on, login_name, password, account_role)
    VALUES (0, 'system', current_timestamp, 0, 'system', current_timestamp, 
            'admin', 
            'QwjnxejpelAVcSh4Ye77LHE1jm68LVM6e2aUDXLfnkg=',
            'ADMIN');
INSERT INTO arc_account(
            arc_account_id, created_by, created_on, is_trashed, updated_by, 
            updated_on, login_name, password, account_role)
    VALUES (1, 'system', current_timestamp, 0, 'system', current_timestamp, 
            'hdfc-user1', 
            'QwjnxejpelAVcSh4Ye77LHE1jm68LVM6e2aUDXLfnkg=',
            'MERCHANT');
INSERT INTO arc_account(
            arc_account_id, created_by, created_on, is_trashed, updated_by, 
            updated_on, login_name, password, account_role)
    VALUES (2, 'system', current_timestamp, 0, 'system', current_timestamp, 
            'jignesh', 
            'QwjnxejpelAVcSh4Ye77LHE1jm68LVM6e2aUDXLfnkg=',
            'CITIZEN');
INSERT INTO arc_account(
            arc_account_id, created_by, created_on, is_trashed, updated_by, 
            updated_on, login_name, password, account_role)
    VALUES (3, 'system', current_timestamp, 0, 'system', current_timestamp, 
            'sangeeth', 
            'QwjnxejpelAVcSh4Ye77LHE1jm68LVM6e2aUDXLfnkg=',
            'CITIZEN');
INSERT INTO arc_account(
            arc_account_id, created_by, created_on, is_trashed, updated_by, 
            updated_on, login_name, password, account_role)
    VALUES (4, 'system', current_timestamp, 0, 'system', current_timestamp, 
            'anil', 
            'QwjnxejpelAVcSh4Ye77LHE1jm68LVM6e2aUDXLfnkg=',
            'CITIZEN');
INSERT INTO arc_account(
            arc_account_id, created_by, created_on, is_trashed, updated_by, 
            updated_on, login_name, password, account_role)
    VALUES (5, 'system', current_timestamp, 0, 'system', current_timestamp, 
            'karthik', 
            'QwjnxejpelAVcSh4Ye77LHE1jm68LVM6e2aUDXLfnkg=',
            'CITIZEN');

