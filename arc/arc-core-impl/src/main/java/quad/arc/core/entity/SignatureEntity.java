package quad.arc.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import quad.arc.core.model.SignatureType;
import quad.arc.core.util.AbstractEntity;

@Entity
@Table(name="ARC_SIGNATURE", 
       uniqueConstraints={@UniqueConstraint(name="UNIQUE_CONSTRAINT",
                                            columnNames={"ARC_CITIZEN_ID","SIGNATURE_NAME", "IS_TRASHED", "UPDATED_ON"})})
public class SignatureEntity extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Long id;
	
    private String name;
    
    private String uri;
    
    private SignatureType signatureType;

    private CitizenEntity citizen;
    
	public SignatureEntity() {
		super();
	}

    @Column(name="ARC_SIGNATURE_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="SIGNATURE_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="SIGNATURE_URI")
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Column(name="SIGNATURE_TYPE")
    @Enumerated(EnumType.STRING)
    public SignatureType getSignatureType() {
        return signatureType;
    }

    public void setSignatureType(SignatureType signatureType) {
        this.signatureType = signatureType;
    }

    @ManyToOne
    @JoinColumn(name="ARC_CITIZEN_ID")
    public CitizenEntity getCitizen() {
        return citizen;
    }

    public void setCitizen(CitizenEntity citizen) {
        this.citizen = citizen;
    }
}
