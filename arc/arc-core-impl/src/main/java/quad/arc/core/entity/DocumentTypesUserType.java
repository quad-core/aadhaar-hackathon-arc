package quad.arc.core.entity;

import quad.arc.core.hibernate.AbstractEnumSetUserType;
import quad.arc.core.model.DocumentType;

public class DocumentTypesUserType extends AbstractEnumSetUserType<DocumentType>{

    public DocumentTypesUserType() {
        super(DocumentType.class);
    }
} 