package quad.arc.core.model;

import quad.arc.core.util.DataBean;

public class Account extends DataBean {
    private static final long serialVersionUID = 1L;
    
    private String loginName;
    
    private String password;
    
    private Role role;

    public Account() {
        super();
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role type) {
        this.role = type;
    }
}
