package quad.arc.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import quad.arc.core.model.DocumentStoreRequest;

@Path("/documents")
@Produces("application/json")
@Consumes("application/json")
public class DocumentsResource {
	public DocumentsResource() {
		super();
	}
	
    @POST
	public Response store(DocumentStoreRequest request) {
		return Response.ok().status(Status.CREATED).build();
	}
}