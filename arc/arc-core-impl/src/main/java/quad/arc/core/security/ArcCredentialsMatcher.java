package quad.arc.core.security;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

public class ArcCredentialsMatcher extends SimpleCredentialsMatcher {
    @Override
    protected Object getCredentials(AuthenticationToken token) {
        char [] password = (char[])super.getCredentials(token);
        String hash = ArcSecurityUtils.hash(new String(password));
        return hash.toCharArray();
    }
}
