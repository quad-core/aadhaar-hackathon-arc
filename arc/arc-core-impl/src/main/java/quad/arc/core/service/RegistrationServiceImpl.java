package quad.arc.core.service;

import org.springframework.beans.factory.annotation.Autowired;

import quad.arc.core.dao.AccountDao;
import quad.arc.core.entity.AccountEntity;
import quad.arc.core.entity.CitizenEntity;
import quad.arc.core.model.Account;
import quad.arc.core.model.Citizen;
import quad.arc.core.util.DataUtil;

public class RegistrationServiceImpl implements RegistrationService {
    
    @Autowired
    private AccountDao accountDao;
    
//    @Autowired
//    private CitizenDao citizenDao;

    public RegistrationServiceImpl() {
        super();
    }

    @Override
    public void registerCitizen(Citizen citizen) {
        AccountEntity accountEntity = new AccountEntity();
        Account account = citizen.getAccount();
        accountEntity.setLoginName(account.getLoginName());
        accountEntity.setPassword(account.getPassword());
        accountEntity.setRole(account.getRole());
        DataUtil.setAuditDetails(accountEntity);
        
        CitizenEntity citizenEntity = new CitizenEntity();
        citizenEntity.setAadhaarNo(citizen.getAadhaarNo());
        citizenEntity.setAccount(accountEntity);
        citizenEntity.setEmailAddress(citizen.getEmailAddress());
        citizenEntity.setMobileNumber(citizen.getMobileNumber());
        citizenEntity.setName(citizen.getName());
        DataUtil.setAuditDetails(accountEntity);
        
//        this.citizenDao.saveCitizen(citizenEntity);
    }
}
