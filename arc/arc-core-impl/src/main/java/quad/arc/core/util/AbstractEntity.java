package quad.arc.core.util;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.hibernate.annotations.Type;

@MappedSuperclass
abstract public class AbstractEntity implements Serializable, Entity {
    private static final long serialVersionUID = 1L;
    
    private boolean trashed;
    
    private Date createdOn;

    private String createdBy;

    private Date updatedOn;

    private String updatedBy;

    public AbstractEntity() {
        super();
        this.trashed = false;
    }

    @Type(type="org.hibernate.type.NumericBooleanType")
    @Column(name="IS_TRASHED",nullable=false)
    public boolean isTrashed() {
        return trashed;
    }

    public void setTrashed(boolean trashed) {
        this.trashed = trashed;
    }

    @Column(name="CREATED_ON")
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Column(name="CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name="UPDATED_ON")
    @Version
    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Column(name="UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
