package quad.arc.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import quad.arc.core.util.AbstractEntity;

@Entity
@Table(name="ARC_MERCHANT_USER", 
       uniqueConstraints={@UniqueConstraint(name="UNIQUE_CONSTRAINT",
                                            columnNames={"ARC_MERCHANT_ID", "USER_NAME", "IS_TRASHED", "UPDATED_ON"})})
public class MerchantUserEntity extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private Long id;
	
    private String name;

    private String mobileNumber;
    
    private String emailAddress;
    
    private AccountEntity account;
    
    private MerchantEntity merchant;

	public MerchantUserEntity() {
		super();
	}

    @Column(name="ARC_MERCHANT_USER_ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="USER_NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="MOBILE_NO")
    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Column(name="EMAIL_ADDRESS")
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="ARC_ACCOUNT_ID", nullable=false)
    @OnDelete(action=OnDeleteAction.CASCADE)
    @PrimaryKeyJoinColumn
    public AccountEntity getAccount() {
        return account;
    }

    public void setAccount(AccountEntity account) {
        this.account = account;
    }

    @ManyToOne
    @JoinColumn(name="ARC_MERCHANT_ID")
    public MerchantEntity getMerchant() {
        return merchant;
    }

    public void setMerchant(MerchantEntity merchant) {
        this.merchant = merchant;
    }
}
